
# Reportes Geoagro

El objetivo de este nodo es procesar reportes a partir de json's de entrada y almacenarlos en google Drive. 
Para procesar cada reporte escucha una cola AWS, procesa el json de entrada, selecciona el tipo de reporte, adapta los datos, selecciona la funcion determinada del reporte y los envia a `Google Apps Script` (**GAS**). GAS recive los datos crea el reporte, lo guarda en el Drive respectivo, y finaliza el workflow enviando la url del reporte generado al orquestador.

## Descripcion

### Gdocs C#
La implementacion esta creada a partir de un patron de diseño ***Factory***. Este patron busca tipificar objetos comunes y tener una fabrica creadora de objectos de un mismo tipo. 

El `main` esta configurado con un ciclo infinito que escucha a la cola correspondiente de AWS. Al entrar un mensaje la funcion `Read` de `Program.cs` lee el msj y crea el reporte requerido a partir de la clase `ReportFactory`.

Para utilizar el patron Factory se crearon las clases `ReportFactory`, `AbstractReport` y los distintos tipos de reportes con su clase propia (`ChangeRateReport`, `ZonesNDVIReport`, ...).
**ReportFactory**: Clase encargada de crear el reporte requerido con la funcion `CreateReport(message)`. Utiliza los permisos de google mediante el metodo `get_scriot_service`
- `CreateReport`: Recive el json de entrada desde `Program.cs`, selecciona el tipo de reportes, crea un *objeto* especifico de ese reporte (Ej: `new ZonesNDVIReport()`) y crea el reporte. Dicho *objeto* transoforma los datos para GAS, envia los datos a GAS, y por ultimo  crea el reporte devolviendo la URL.
  
**AbstractReports**: Objeto abstracto encargado de tipificar los reportes. Su funcion prinicipal es estanadarizar y reutilizar codigo para la generacion de los distintos reportes. Todos los reportes heradan esta clase por lo que tambien posee funciones de huso comun. Dentro de sus propiedades estan:
- *GasFunction*: Nombre de la funcion que sera llamada en GAS
- *ConsoleFlag*: Flag visualizado en implementacion `"[3/4]Generating {reportName}.\tPlease, wait..."`
- *InputMessage*: json de entrada desde amazon

### Google Apps Script:
Scripts encontrados en el proyecto `ejercicio 6b` en Drive de la cuenta `report@geoagro.com`. Para testing se encuentra el proyecto `ScriptDev`. Ambos poseen distintos scripts para la generacion de los reportes (Ej: `rpt_SeriesThumbs.gs` , `rpt_ZonesNDVI.gs`, ...) Cada reporte posee sus datos constantes en su archivo de configuracion siendo `rpt_ZonesNdvi.gs` **`->`** `rpt_ZonesNdviConfig.gs`. Para las funciones de huso comun se definio `common_funcs.gs`
> Nota: En cada proyecto cada variable o funcion que se nombre debe tener `nombre unico`, independiente del nombre  del script. El nombre del script solo sirve para ordenarlos, es decir, si declaramos la variable global `a` en el script `rpt_ZonesNdvi.gs`, estara tambien declarada en `rpt_SeriesThumbs.gs`

#### Scripts y Estandares Adoptados
- *rpt_{ReportName}.gs*: En este script se encuentra el metodo de contruccion del reporte `build_{ReportName}` que sera ejecutado desde la funcion de testeo `test_build_{ReportName}` con datos hardcodeados, o desde la funcion llamada del nodo **Gdocs** `do_report_{ReportName}`. Todos los reportes estan conformados por distintas tablas, cada tabla de datos tiene su funcion propia. Ejemplo: sea la tabla titulada *"Mapa de Lotes y Cultivos"*, habra una funcion que seteara esta tabla llamada `rpt_{ReportName}_setMapaLotesYCultivos`

- *rpt_{ReportName}Config.gs*: Script para definir strings constantes dependiendo del lenguaje, datos hardcodeados de prueba, estado debuger, id's de plantillas de google docs y spreadsheets, tamaños de imagenes y formatos de fechas. Todos estos datos se encuentran encapsulados en un objeto `{reportName}Config` (Ej `zonesConfig`) para evitar el renombramiento de variables.