getDataFrame <- function(myDF){ 
  indexs.data <- data.frame(ndvi = numeric(),
                            name = character(),
                            cultivo = character(),
                            color = character())
  for(i in 1:nrow(myDF)){
    LocalIndex.data <- data.frame(
        ndvi        = c(myDF[i,]$min,
                        myDF[i,]$firstQt,
                        myDF[i,]$median, 
                        myDF[i,]$thirdQt, 
                        myDF[i,]$max),
        name        = rep(x = myDF[i,]$name, 5),
        cultivo     = rep(x = myDF[i,]$cultivo, 5),
        color       = rep(x = myDF[i,]$color, 5)
    )
    indexs.data <- rbind(indexs.data, LocalIndex.data)
  }
  return(indexs.data)
}

getOrderedColors <- function(DF){
  suppressMessages(attach(DF));
  orderedDF<-DF[order(cultivo),]
  colorsOrdered <- as.character(orderedDF[!duplicated(orderedDF$cultivo),]$color)
  return(colorsOrdered)  
}

suppressPackageStartupMessages(require('ggplot2'))
suppressPackageStartupMessages(library('ggplot2'))

#pathWithCsvFile pasada por C# code
#imageName       pasada por C# code
#Ylabel          pasada por C# code
#pathWithCsvFile <- "C:/Users/54343/Source/Repos/Reportes/mynet_gdocs/create_google_doc/Rnode/Rscripts/prueba1.csv"   ## ONLY DEBUG MODE 
#imageName       <- "C:/Users/54343/Source/Repos/Reportes/mynet_gdocs/create_google_doc/Rnode/Rscripts/imagePlot.png" ## ONLY DEBUG MODE
#Ylabel          <- "NDVI" ## ONLY DEBUG MODE
 
print(paste("Boxplot index : ", Ylabel, sep=" "));
myCsv = read.csv(pathWithCsvFile, header = TRUE) #,stringsAsFactors = TRUE
reorderedDF <- getDataFrame(myCsv)
colorsOrdered <- getOrderedColors(myCsv)
# suppressMessages(colorsOrdered <- getOrderedColors(myCsv))

#Crear y mostrar imagen
#Con valores 
p <- ggplot(reorderedDF, aes(x=name, y=ndvi, fill=cultivo)) +  geom_boxplot(color = "#8F89A2", alpha=0.5) + ylab(Ylabel) + xlab("") + coord_flip() + theme_bw() + stat_summary(geom="text", fun.y=quantile, aes(label=sprintf("%1.2f", ..y..)), colour = "#B3B3B3", position=position_nudge(x=0.5), size=3.5)
p + scale_fill_manual(values=colorsOrdered)
png(imageName) #, width = 400, height = 400)
print(p + scale_fill_manual(values=colorsOrdered))
dev.off()
#Sin valores
#p <- ggplot(reorderedDF, aes(x=name, y=ndvi, fill=cultivo)) +  geom_boxplot(color = "#8F89A2", alpha=0.5)+ xlab("") + coord_flip() + theme_bw() 
#p + scale_fill_manual(values=colorsOrdered)

#boxplot(c(1, 2, 2, 3, 4, 5, 6))                                         ##DEBUG MODE
#boxplot(c(1, 2, 1, 1, 1, 2, 3, 4, 5, 6))                                ##DEBUG MODE