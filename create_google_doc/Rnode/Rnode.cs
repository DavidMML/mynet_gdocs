﻿//http://inut-santa.blogspot.com/2017/01/executing-r-script-files-from-c-with.html
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using create_google_doc.Rnode.DataFrame;
using RDotNet;

namespace create_google_doc.Rnode
{
    public class Rnode
    {
        private static bool debugMode;
        private static dynamic inputMsg;
        private static string baseDirectory;
        private static string rpath;
        private static string rscriptFile;
        private static string scriptpath;
        private static string csvPath = "";
        private static string imageName;
        private static string imageNamePath;


        //public static void Main(string[] args){
        //    string inputTestMessage = "\\Test\\inputBoxplotIndex.json";
        //    var debug = true;
        //    setConfigs(debug);
        //    inputMsg = getData( baseDirectory + inputMsg)
        //    mainRNode(inputTestMessage);
        //}
        public static string GetRChart(dynamic jsonData, string imageName, string Ylabel) {
            var debug = false;
            setConfigs(debug, imageName);
            inputMsg = jsonData;
            return mainRNode(Ylabel);
        }

        public static void setConfigs(bool debug, string imageName){
            debugMode = debug;
            baseDirectory = System.AppContext.BaseDirectory.Replace("\\bin\\Debug\\netcoreapp2.1\\", "") + @"\Rnode";//modificar para que encuentre la version
            //baseDirectory = System.AppContext.BaseDirectory.Replace("\\bin\\Debug\\netcoreapp2.2\\", "");
            rpath = @"C:\Program Files\R\R-3.5.3\bin\x64";
            rscriptFile = @"\Rscripts";
            scriptpath = baseDirectory + rscriptFile + @"\boxPlotsIndex.R";
            csvPath = baseDirectory + rscriptFile + @"\prueba1.csv";
            imageName = imageName + ".png";
            imageNamePath = baseDirectory + @"\" + imageName;
        }

        public static string mainRNode(string Ylabel){
            DeleteFile(csvPath);
            DeleteFile(imageNamePath);
            //getting data to make image

            dynamic data = inputMsg;
            Console.Write ("\n\t*[1/4] Creating R Data Frame...             ");
            var DFD = new DataFrameDelegator(data);
            Console.Write("Done");
            //write csv file
            CsvFileWriter(DFD.MyDF.ColumnTitles, DFD.MyDF.Data, csvPath);
            //run R script and generate image
            runRscript(rpath, scriptpath, Ylabel);
            // //convert to string 64 bits
            string image64 = getImage64bits(imageNamePath);
            //Console.WriteLine(image64);
            Console.Write ("\n\t*[4/4]   ---- Image base 64 generated ----");
            Console.Write ("\n                                                       ");
            return image64;
        }

        private static REngine engine;
        public static void runRscript(string rpath, string scriptFilePath, string Ylabel){
            Console.Write ("\n\t*[3/4] Running R script...                  \n           ");
            scriptFilePath = scriptFilePath.Replace('\\', '/');
            var csvPathModif = csvPath.Replace('\\', '/');
            var imageNamePathModif = imageNamePath.Replace('\\', '/');

            if(engine == null){
                engine = REngine.GetInstance();
                engine.Initialize();
            }
            engine.Evaluate("pathWithCsvFile <-" + "\"" + csvPathModif       + "\"");
            engine.Evaluate("imageName <-"       + "\"" + imageNamePathModif + "\"");
            engine.Evaluate("Ylabel <-"          + "\"" + Ylabel + "\"");
            engine.Evaluate("source('" + scriptFilePath + "')");

            Console.Write("                                                    Done");   
        }

        public static void CsvFileWriter(string Titles, List<string> Data, string csvPath) {
            Console.Write("\n\t*[2/3] Writing Csv File...                  ");
            File.WriteAllText(csvPath, Titles); 
            foreach (var i in Data){
                File.AppendAllText(csvPath, i);
            }
            string readText = File.ReadAllText(csvPath);
            //Console.WriteLine(readText);
            Console.Write("Done");
        }

        public static string getImage64bits(string imagePath){
            string base64ImageRepresentation = "";
            for (int i = 0; i < 100; i++){
                try{
                    byte[] imageArray = System.IO.File.ReadAllBytes(imagePath);
                    base64ImageRepresentation = Convert.ToBase64String(imageArray);    
                }catch (System.Exception){
                    Console.Write(0.5 * i + "  ");
                    Thread.Sleep(500);
                }
            }
            return base64ImageRepresentation;
        }

        public static dynamic getData(string inputMsgPath){// method will be updated to call AWS queue method
            Console.Write("\n\t*[1/3]Getting data...                         ");
            var msg = File.ReadAllText (inputMsgPath);//Debug model
            Console.Write("Done");
            return msg;
        }

        public static void DeleteFile(string pathFile){
            if(System.IO.File.Exists(pathFile)){
                try{
                    System.IO.File.Delete(pathFile);
                }catch (System.IO.IOException e){
                    Console.WriteLine(e.Message);
                    return;
                }
            }
        }
    }
}
