using System;
using System.Collections.Generic;

namespace create_google_doc.Rnode.DataFrame.GraphicType
{
    public class BoxplotIndex : AbstractDataFrame
    {
        public BoxplotIndex(dynamic inputMessage) {
            this.inputMsg = inputMessage;
            this.SetColumnTitles();
            this.SetData();
        }

        protected override void SetColumnTitles() {
            this.ColumnTitles = "min" + delimiter +
                                "firstQt" + delimiter +
                                "median" + delimiter +
                                "thirdQt" + delimiter +
                                "max" + delimiter +
                                "name" + delimiter +
                                "cultivo" + delimiter +
                                "color" + 
                                Environment.NewLine;  
        }

        protected override void SetData() {
            this.Data = new List<string>();
            if (this.inputMsg != null) { 
                foreach (var i in this.inputMsg){
                    string data = (string)i.distributionData[0] + delimiter +
                                  (string)i.distributionData[1] + delimiter +
                                  (string)i.distributionData[2] + delimiter +
                                  (string)i.distributionData[3] + delimiter +
                                  (string)i.distributionData[4] + delimiter +
                                  (string)i.name + delimiter +
                                  (string)i.cultivo + delimiter +
                                  (string)i.color + 
                                  Environment.NewLine;
                    this.Data.Add(data);
                }
            }else { 
                // messageError("Lotes not found on .json input ")
            }
        }
    }
}
//Example
//min	firstQt	median	thirdQt	max	    name	cultivo color	
//0.2	0.3	    0.4	    0.72	0.9	    Lote1	Soja	#D8668A	
//0.32	0.52	0.61	0.69	0.75    Lote2   Soja	#D8668A	
//0.15	0.21	0.51	0.54	0.68	Lote32  Soja	#D8668A	
//0.05	0.12	0.32	0.48	0.63	Lote5   Maiz	#D8668A	
//0.02	0.15	0.42	0.7	    0.81	Lote5   Maiz	#D8668A	
