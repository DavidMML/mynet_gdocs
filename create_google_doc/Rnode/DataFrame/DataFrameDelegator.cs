using create_google_doc.Rnode.DataFrame.GraphicType;

namespace create_google_doc.Rnode.DataFrame
{
    public class DataFrameDelegator
    {
        public AbstractDataFrame MyDF { get; set; }
        public DataFrameDelegator(dynamic inputMessage)
        {
            //var inputMsg = JsonConvert.DeserializeObject(inputMessage);
            //string graphicType = (string) inputMsg.Graphic;
            string graphicType = (string) inputMessage.Graphic;
            switch (graphicType){
                //case "boxplot_index" : MyDF = new BoxplotIndex(inputMsg.Lotes); break;
                case "boxplot_index" : MyDF = new BoxplotIndex(inputMessage.Lotes); break;
                // default:               ErrorMsg("Graphic type invalid name");
            }
        }
    }
}