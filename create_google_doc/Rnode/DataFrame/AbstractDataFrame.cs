using System;
using System.Collections.Generic;

namespace create_google_doc.Rnode.DataFrame
{
    public abstract class AbstractDataFrame
    {
        public string delimiter = ",";
        public dynamic inputMsg { get; set; }
        public String ColumnTitles { get; set; }
        public List<String> Data { get; set; }

        // public AbstractDataFrame(dynamic inputMessage) {
        //     this.inputMsg = inputMessage;
        // }
        protected virtual void SetColumnTitles() { }
        protected virtual void SetData(){ }
    }   
}