﻿using Amazon;
using Amazon.Runtime;
using Amazon.SQS;
using Amazon.SQS.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace create_google_doc
{
    class SQSQueue
    {
        private IAmazonSQS client;
        private string queueUrl;

        public SQSQueue()
        {
            client = GetClient();
        }

        public IAmazonSQS GetClient()
        {
            Dictionary<string, string> val = GetConfigs();

            var sqsConfig = new AmazonSQSConfig();
            sqsConfig.ServiceURL = "http://sqs." + val["3"] + ".amazonaws.com";

            return new AmazonSQSClient(new BasicAWSCredentials(val["1"], val["2"]), sqsConfig);
        }

        public Dictionary<string, string> GetConfigs()
        {
            string jsonStr = Encoding.Default.GetString(Properties.Resources.configs);
           
            return JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(jsonStr)[0];
        }
        
        public async Task CreateQueueIfNotExists(string queueName)
        {
            CreateQueueRequest sqsRequest = new CreateQueueRequest
            {
                QueueName = queueName
            };

            Console.Write($"Creating SQS queue (if not exists) '{queueName}'...");
            await client.CreateQueueAsync(sqsRequest);
            Console.WriteLine("\tDone.");

            return;
        }

        public async Task<string> Use(string queueName)
        {
            GetQueueUrlRequest getQueueUrlRequest = new GetQueueUrlRequest
            {
                QueueName = queueName
            };

            var response = await client.GetQueueUrlAsync(getQueueUrlRequest);
            queueUrl = response.QueueUrl;

            return queueUrl;
        }

        public async Task SendMessage(string message, string queueurl)
        {
            SendMessageRequest sendMessage = new SendMessageRequest
            {
                QueueUrl = queueurl,
                MessageBody = message
            };
        
            await client.SendMessageAsync(sendMessage);

            return;
        }

        public async Task<int> GetMessageCount()
        {
            var attrs = await client.GetAttributesAsync(queueUrl);
            string count;
            attrs.TryGetValue("ApproximateNumberOfMessages", out count);

            return int.Parse(count);
        }

        public async Task<ReceiveMessageResponse> GetMessage(int visibilityTimeout)
        {
            ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest
            {
                QueueUrl = queueUrl,
                MaxNumberOfMessages = 1,
                VisibilityTimeout = (int) TimeSpan.FromMinutes(visibilityTimeout).TotalSeconds
            };

            return await client.ReceiveMessageAsync(receiveMessageRequest);
        }

        public async Task<ReceiveMessageResponse> GetMessages()
        {
            ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest
            {
                QueueUrl = queueUrl
            };
            
            return await client.ReceiveMessageAsync(receiveMessageRequest);
        }

        public async Task DeleteMessage(string receiptHandle)
        {
            DeleteMessageRequest deleteMessageRequest = new DeleteMessageRequest
            {
                QueueUrl = queueUrl,
                ReceiptHandle = receiptHandle
            };

            await client.DeleteMessageAsync(deleteMessageRequest);

            return;
        }

        public async Task CleanQueue()
        {
            PurgeQueueRequest purgeQueueRequest = new PurgeQueueRequest
            {
                QueueUrl = queueUrl
            };

            Console.Write("Purging SQS queue...");
            await client.PurgeQueueAsync(purgeQueueRequest);
            Console.WriteLine("\tDone.");

            return;
        }
    }
}
