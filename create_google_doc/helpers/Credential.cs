﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Auth.OAuth2.Flows;
using Google.Apis.Auth.OAuth2.Responses;
using Newtonsoft.Json;
using System.IO;
using System.Net;
using System.Text;
using System.Web;

namespace create_google_doc
{
    public class Credential
    {
        public static UserCredential get_user_credential(
            string google_email,
            string google_client_id,
            string google_client_secret,
            string google_user_id,
            string google_refresh_token,
            string applicationName
        )
        {
            GoogleRefreshTokenResponse resp = GetNewAccessToken(
                google_client_id,
                google_client_secret,
                google_refresh_token
            );

            string google_access_token = resp.access_token;

            var tokenResponse = new TokenResponse
            {
                AccessToken = google_access_token,
                RefreshToken = google_refresh_token,
            };

            var apiCodeFlow = new GoogleAuthorizationCodeFlow(
                new GoogleAuthorizationCodeFlow.Initializer
                {
                    ClientSecrets = new ClientSecrets
                    {
                        ClientId = google_client_id,
                        ClientSecret = google_client_secret
                    }//,
                    //Scopes = new[] { ScriptService.Scope.Drive, DriveService.Scope.Drive },
                    //DataStore = new FileDataStore(applicationName)
                }
            );

            var credential = new UserCredential(apiCodeFlow, google_email, tokenResponse);

            return (credential);
        }

        private static GoogleRefreshTokenResponse GetNewAccessToken(
            string google_client_id, 
            string google_client_secret, 
            string google_refresh_token
        )
        {
            // https://msdn.microsoft.com/en-us/library/ff752395.aspx
            // https://stackoverflow.com/questions/10631042/how-to-generate-access-token-using-refresh-token-through-google-drive-api
            string requestUrl = "https://www.googleapis.com/oauth2/v4/token";

            StringBuilder postData = new StringBuilder();

            postData.Append("client_id=" + HttpUtility.UrlEncode(google_client_id) + "&");
            postData.Append("client_secret=" + HttpUtility.UrlEncode(google_client_secret) + "&");
            postData.Append("refresh_token=" + HttpUtility.UrlEncode(google_refresh_token) + "&");
            postData.Append("grant_type=" + HttpUtility.UrlEncode("refresh_token"));

            ASCIIEncoding ascii = new ASCIIEncoding();
            byte[] postDataEncoded = ascii.GetBytes(postData.ToString());

            WebRequest req = HttpWebRequest.Create(requestUrl);

            req.Method = "POST";
            req.ContentType = "application/x-www-form-urlencoded";
            req.ContentLength = postDataEncoded.Length;

            Stream requestStream = req.GetRequestStream();
            requestStream.Write(postDataEncoded, 0, postDataEncoded.Length);

            WebResponse res = req.GetResponse();

            string responseBody = null;

            using (StreamReader sr = new StreamReader(res.GetResponseStream(), Encoding.UTF8))
            {
                responseBody = sr.ReadToEnd();
            }
            
            var refreshResponse = JsonConvert.DeserializeObject<GoogleRefreshTokenResponse>(responseBody);
            return refreshResponse;
        }
    }

    class GoogleRefreshTokenResponse
    {
        public string access_token { get; set; }
        public string token_type { get; set; }
        public long expires_in { get; set; }
        public string id_token { get; set; }
    }

}
