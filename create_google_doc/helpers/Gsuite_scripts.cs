﻿
using Google.Apis.Auth.OAuth2;
using Google.Apis.Script.v1;
using Google.Apis.Script.v1.Data;
using Google.Apis.Services;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace create_google_doc
{
    public class Gsuite_scripts
    {
        private string _scriptId;

        public Gsuite_scripts(String scriptId)
        {
            _scriptId = scriptId;
        }

        // Create Scripts API service.
        public static ScriptService get_script_service(UserCredential userCredential)
        {
            string ApplicationName = string.Empty;
            ScriptService service = null;

            ApplicationName = "Script API for current user";
            service =
                new ScriptService (
                    new BaseClientService.Initializer()
                    {
                        //  DefaultExponentialBackOffPolicy = Google.Apis.Http.ExponentialBackOffPolicy.None,
                        HttpClientInitializer = userCredential,
                        ApplicationName = ApplicationName,
                    }
                );

            service.HttpClient.Timeout = TimeSpan.FromMinutes(15);
            return (service);
        }


        public async Task<JObject> execute_function(ScriptService service, string function_name, List<object> parameters)
        {
            // Create an execution request object.
            ExecutionRequest request = new ExecutionRequest();
            request.DevMode = false;
            request.Function = function_name;// "getFoldersUnderRoot";

            if (parameters != null)
            {
                request.Parameters = parameters;
            }

            ScriptsResource.RunRequest runReq = service.Scripts.Run(request, _scriptId);

            try
            {
                // Make the API request.
                Operation op = await runReq.ExecuteAsync();

                if (op.Error != null)
                {
                    // The API executed, but the script returned an error.

                    // Extract the first (and only) set of error details
                    // as a IDictionary. The values of this dictionary are
                    // the script's 'errorMessage' and 'errorType', and an
                    // array of stack trace elements. Casting the array as
                    // a JSON JArray allows the trace elements to be accessed
                    // directly.
                    IDictionary<string, object> error = op.Error.Details[0];
                    string err = error["errorMessage"].ToString();

                    try
                    {
                        if (error["scriptStackTraceElements"] != null)
                        {
                            // There may not be a stacktrace if the script didn't
                            // start executing.
                            //Console.WriteLine("Script error stacktrace:");
                            JArray st = (JArray)error["scriptStackTraceElements"];

                            foreach (var trace in st)
                            {
                                err += trace["function"] + ", " + trace["lineNumber"];
                            }
                        }
                    }
                    catch 
                    {
                        ; //do thing
                    }
                    throw new Exception(err);
                }
                else
                {
                    // The result provided by the API needs to be cast into
                    // the correct type, based upon what types the Apps
                    // Script function returns. Here, the function returns
                    // an Apps Script Object with String keys and values.
                    // It is most convenient to cast the return value as a JSON
                    // JObject (folderSet).
                    JObject result = (JObject)op.Response["result"];

                    return (result);
                }
            }
            catch (Exception e)// (Google.GoogleApiException e)
            {
                // The API encountered a problem before the script
                // started executing.
                throw new Exception("error", e);
            }
        }
    }
}
