using System.Linq;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Script.v1;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace create_google_doc.ReportsFactory
{
    public class ReportsFactory
    {
        //https://developers.google.com/apps-script/api/how-tos/execute
        private static ReportsFactory instance = new ReportsFactory();
        public static ReportsFactory Instance => instance;
        private List<reportsScript> gasScripts = new List<reportsScript>();
        private struct reportsScript {
            public string Id { get; set; }
            public string Name { get; set; }
        }

        public ReportsFactory(){
            reportsScript rs = new reportsScript();
            rs.Name = "Production";
            rs.Id   = "MXu7l96bCWYW20e2Ob127zA2mVFSXGdDy";
            this.gasScripts.Add(rs);
            rs.Name = "Development";
            rs.Id   = "MXu7l96bCWYW20e2Ob127zA2mVFSXGdDy";
            this.gasScripts.Add(rs);
            rs.Name = "Development2";
            rs.Id   = "Mo62XMlcOYhtzEn6H5ZI4yQ2mVFSXGdDy";
            this.gasScripts.Add(rs);
        }

        public ScriptService get_script_service(dynamic credentials){
            // los siguientes parametros deberian ser provistos por BI al orquestador, pero para este ejemplo los hardcodeamos
            string applicationName = "test1";
            string google_email = "";
            string google_client_id = "";
            string google_client_secret = "";
            string google_user_id = "";
            string google_refresh_token = "";

            #if BI
                // en el caso que este codigo se corra en BI, las credenciales se leen de la base de datos:
                geo_ui.Helpers.dbHelper.get_oauth_credentials(out google_client_id, out google_client_secret);
                string bi_userId = geo_ui.Helpers.dbHelper.get_userId_for_email(google_email);
                google_user_id = geo_ui.Helpers.dbHelper.getClaimValue(bi_userId, geo_common.Helper.GoogleUserId);
                google_refresh_token = geo_ui.Helpers.dbHelper.getClaimValue(bi_userId, geo_common.Helper.GoogleRefreshToken);
            #else
                google_email =          (string)credentials.google_email;
                google_client_id =      (string)credentials.google_client_id;
                google_client_secret =  (string)credentials.google_client_secret;
                google_user_id =        (string)credentials.google_user_id;
                google_refresh_token =  (string)credentials.google_refresh_token;
            #endif

            UserCredential cred =
            Credential.get_user_credential(
                google_email,
                google_client_id,
                google_client_secret,
                google_user_id,
                google_refresh_token,
                applicationName
            );

            return Gsuite_scripts.get_script_service(cred);
        }

        public async Task<string> CreateReport(dynamic message, string googleAppsScript){
            Console.WriteLine($"       ** Runing on : {googleAppsScript} (Google Apps Script)");
            // Console.WriteLine("       ** Runing on : " + googleAppsScript + " (Google Apps Script)");
            var currentReportScript = gasScripts.Where(x => x.Name == googleAppsScript).First();
            ScriptService svc = get_script_service(message.input);//salta en error en credeciales
            var scripts = new Gsuite_scripts(currentReportScript.Id);
            List<object> l = new List<object>();
            JObject output = new JObject();
            string URLOfGeneratedReport = "";
            
            string newReportType = (string)message.input.str_function;
            newReportType = newReportType.Split("_")[0];
            Reports.AbstractReport newReport ; 

            switch (newReportType){
                case "indices":      newReport = new Reports.IndexMeanByFieldReport(message); break;
                case "lotes":        newReport = new Reports.LotsAndCropsReport(message);     break;
                case "punto":        newReport = new Reports.PointByAreaReport(message);     break;
                case "recorridos":   newReport = new Reports.CropScoutingReport(message);     break;
                case "serie":        newReport = new Reports.SeriesThumbReport(message);      break;
                case "tasa":         newReport = new Reports.ChangeRateReport(message);       break;
                case "zonificacion": newReport = new Reports.ZonesReport(message);            break;
                default:             newReport = new Reports.ZonesReport(message);            break;
            }
            Console.WriteLine ($"       ** User :      {(string)message.input.google_email}");
            Console.Write("[2/4] Fetching data for report.\t\tPlease, wait...");
            await newReport.createReport();
            l.Add(JsonConvert.SerializeObject(newReport.InputJson));
            l.Add(JsonConvert.SerializeObject(newReport.OutputJson));
            Console.Write(newReport.ConsoleFlag);
            output = await scripts.execute_function(svc, newReport.GasFunction, l);
            Console.WriteLine(" Done.");

            URLOfGeneratedReport = output.GetValue("url").ToString();

            return URLOfGeneratedReport;
        }
    }
}