using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Threading.Tasks;

namespace create_google_doc.ReportsFactory.Reports
{
    public class ChangeRateReport : AbstractReport
    {
        private struct ClaseArea{
                public decimal has;
                public decimal range;
        }
        private readonly string _consoleFlag = "[3/4] Generating Change Rate report.\tPlease, wait...";
        private readonly string _gasFunction = "do_report_TasaDeCambio";

        public ChangeRateReport(dynamic message)
        {
            this.ConsoleFlag = _consoleFlag;
            this.GasFunction = _gasFunction;
            this.InputMessage = message;
        }

        public override async Task createReport()
        {
            // this.GetCsvData();
            this.SetInputJson();
            this.SetOutputJson();
            // await this.GetCsvData();
            Console.WriteLine(" Done.");
        }

        public override void SetInputJson()
        {
            this.InputJson = JsonConvert.DeserializeObject(
                "{  Title : \"" + (string)this.InputMessage.input.json__input.Title + "\"," +
                   "Season : \"" + (string)this.InputMessage.input.json__input.Season + "\"," +
                   "Farm : \"" + (string)this.InputMessage.input.json__input.Farm + "\"," +
                   "Crop : \"" + (string)this.InputMessage.input.json__input.Crop + "\"," +
                   "Hybrid : \"" + (string)this.InputMessage.input.json__input.Hybrid + "\"," +
                   "DatePlanting : \"" + (string)this.InputMessage.input.json__input.DatePlanting + "\"," +
                   "Field : \"" + (string)this.InputMessage.input.json__input.Field + "\"," +
                   "FieldBackgroundImage : \"" + (string)this.InputMessage.input.json__input.FieldBackgroundImage + "\"," +
                   "Language : \"" + (string)this.InputMessage.input.str_language + "\"," +
                   "Index : \"" + (string)this.InputMessage.input.index + "\"," +
                   "DateNDVI1 : \"" + (string)this.InputMessage.input.str_start_date + "\"," +
                   "DateNDVI2 : \"" + (string)this.InputMessage.input.str_end_date + "\"," +
                   "Palette : {} ," +
                   "min_max_tasa : {} ," +
                   "min_max_dif : {} ," +
                   "URLimgTasaCambio: \"\"," +
                   "URLimgTasaIndex: \"\"," +
                   "URLimgNDVI1: \"\", " +
                   "URLimgNDVI2: \"\", " +
                   "DailyChangeRate: {} ," +
                   "VariationIndex : {} }");
            this.InputJson.Palette = JsonConvert.DeserializeObject("{ Name : \"\" , Colors : []}");
            this.InputJson.Palette.Name = (string)this.InputMessage.input.palette.name;
            this.InputJson.Palette.Colors = new JArray(this.InputMessage.input.palette.colors);
            this.InputJson.URLimgTasaCambio = (string)this.InputMessage.input.url_rate;
            this.InputJson.URLimgTasaIndex = (string)this.InputMessage.input.url_diff;
            this.InputJson.URLimgNDVI1 = (string)this.InputMessage.input.url_ndvi1;
            this.InputJson.URLimgNDVI2 = (string)this.InputMessage.input.url_ndvi2;
            this.InputJson.min_max_tasa = JsonConvert.DeserializeObject("{ min : \"\" , max : \"\"}");
            this.InputJson.min_max_tasa.min = (string)this.InputMessage.input.min_max_tasa.min;
            this.InputJson.min_max_tasa.max = (string)this.InputMessage.input.min_max_tasa.max;
            this.InputJson.min_max_dif = JsonConvert.DeserializeObject("{ min : \"\" , max : \"\"}");
            this.InputJson.min_max_dif.min = (string)this.InputMessage.input.min_max_dif.min;
            this.InputJson.min_max_dif.max = (string)this.InputMessage.input.min_max_dif.max;

            this.InputJson.DailyChangeRate = JsonConvert.DeserializeObject(
                "{ Has: [], " +
                 " Ranges: []}"
            );
            List<ClaseArea> claseAreaList = getHasRange(this.InputMessage.input.clase_area_rate);
            foreach (var i in claseAreaList){
                this.InputJson.DailyChangeRate.Has.Add(i.has);
                this.InputJson.DailyChangeRate.Ranges.Add(i.range);
            }

            this.InputJson.VariationIndex = JsonConvert.DeserializeObject(
                "{ Has: [], " +
                 " Ranges: []}"
            );
            claseAreaList = getHasRange(this.InputMessage.input.clase_area_diff);
            foreach (var i in claseAreaList){
                this.InputJson.VariationIndex.Has.Add(i.has);
                this.InputJson.VariationIndex.Ranges.Add(i.range);
            }
        }

        private List<ClaseArea> getHasRange(dynamic input){
            var claseArea = new List<ClaseArea>();
            foreach (JProperty i in input){
                ClaseArea ca = new ClaseArea() { 
                    range = decimal.Parse(i.Name, CultureInfo.InvariantCulture),
                    has = decimal.Parse((string)i.Value, CultureInfo.InvariantCulture)
                };
                claseArea.Add(ca);
            }
            return claseArea.OrderByDescending(x => x.range).ToList();
        }
        
        public override void SetOutputJson(){}
    }
}
