using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace create_google_doc.ReportsFactory.Reports
{
    public class SeriesThumbReport : AbstractReport
    {
        // private int _vistaCount;
        private string _newReportTypeName;
        private int _amountOfDates;
        private int _amountOfFields;
        private string[] _lotData;

        private static string _thumbnailsType;//static?
        private readonly string _consoleFlag = $"[4/5] Generating Thumbs {_thumbnailsType} report.\tPlease, wait...";
        private readonly string _gasFunction = "do_report_SeriesThumbs";

        public SeriesThumbReport(dynamic message){
            this.ConsoleFlag = _consoleFlag;
            this.GasFunction = _gasFunction;
            this.InputMessage = message;

            this._newReportTypeName = ((string)this.InputMessage.input.str_function).Split("_")[1].ToUpper(); 
            this._amountOfDates = this.InputMessage.input.json__input.Date_list.Count;       
            this._amountOfFields = this.InputMessage.input.str_geojson_region.features.Count;
        }

        public override async Task createReport(){
            this.SetInputJson();
            await this.getCsvData();
            this.SetOutputJson();
        }

        public override void SetInputJson(){ 
            // Creating dynamic objects.
            List<string> dates = new List<string>();

            this.InputJson = JsonConvert.DeserializeObject ("{Title: \"\",Season: \"\",Farm: \"\", IndexName: \"\"}");
            this.InputJson.Title = (string)this.InputMessage.input.json__input.Title;
            this.InputJson.Season = (string)this.InputMessage.input.json__input.Season;
            this.InputJson.Farm = (string)this.InputMessage.input.json__input.Farm;
            this.InputJson.IndexName = this._newReportTypeName;

            string datesImages = "Date\n";
            for (int i = 0; i < this._amountOfDates; ++i){
                string date = this.InputMessage.input.json__input.Date_list[i];
                dates.Add(date);
                if (i == this._amountOfDates - 1)
                    datesImages += date;
                else
                    datesImages += date + "\n";
            }
            this.InputJson.DatesImages = datesImages;

            this.InputJson.FieldsData = JsonConvert.DeserializeObject("[]");

            string fieldsData = "";
            for (int i = 0; i < this._amountOfFields; ++i){
                dynamic field = this.InputMessage.input.str_geojson_region.features[i];

                string field_name = (string)field.properties.field_name;
                string crop = (string)field.properties.Crop;
                string hybrid = (string)field.properties.Hybrid;
                string seeding_date = (string)field.properties.DatePlanting;

                fieldsData = "Field,Crop,Hybrid,DatePlanting\n" + field_name + "," + crop + "," + hybrid + "," + seeding_date;

                field = JsonConvert.DeserializeObject("{\"FieldsData\": \"" + fieldsData + "\"}");
                this.InputJson.FieldsData.Add(field);
            }
            
            this.InputJson.Language = (string)this.InputMessage.input.str_language;
        }

        public override void SetOutputJson()
        {
            this.OutputJson = JsonConvert.DeserializeObject("[]");
            foreach (string lot in this._lotData){
                this.OutputJson.Add(JsonConvert.DeserializeObject("{\"SeriesValues\": \"" + lot + "\"}"));
            }
            Console.WriteLine(" Done.");
        }
        
        public async Task getCsvData() { 
            List<string> splitted = new List<string>();
            string content = await DownloadData((string)this.InputMessage.input.str_output_uri);
            string[] rows;
            string[] values;

            rows = content.Split('\n');
            bool flag = false;
            int index = 0;
            int rowCount = 0;
            int mult = 1;
            int factor = 0;

            this._lotData = new string[this._amountOfFields];
            List<List<string>> AllLotsImgUrls = new List<List<string>>();
            string[,] AllLotsImgsInBase64 = new string[this._amountOfFields, this._amountOfDates];

            foreach (string row in rows){
                if (!string.IsNullOrWhiteSpace(row)){
                    if (flag){
                        if (rowCount < this._amountOfFields){
                            this._lotData[rowCount] = "Date,Index,ImageURL\n";
                            AllLotsImgUrls.Add(new List<string>(this._amountOfDates));
                        }

                        factor = mult * this._amountOfDates;

                        if (rowCount == factor){
                            index++;
                            mult++;
                        }                      

                        values = row.Split(',');

                        if (values[2] == "") values = this.setImageNotFound(values);

                        AllLotsImgUrls[index].Add(values[3]);                     

                        this._lotData[index] += values[0].Split("T")[0] + "," + values[2] + "," + "[" + index + "-" + (AllLotsImgUrls[index].Count - 1) + "]";
                        if (rowCount != (factor-1))
                            this._lotData[index] += "\n";

                        rowCount++;
                    }else{
                        flag = true;
                    }
                }
            }
            Console.WriteLine(" Done.");

            Console.Write("[3/5] Downloading image resources.\tPlease, wait...");
            // Function that downloads in parallel all the images in the report.
            AllLotsImgsInBase64 = await ParallelizeMatrix(AllLotsImgsInBase64, AllLotsImgUrls);

            for (int i = 0; i < this._amountOfFields; ++i){
                for (int j = 0; j < this._amountOfDates; ++j){
                    this._lotData[i] = this._lotData[i].Replace("[" + i + "-" + j + "]", AllLotsImgsInBase64[i, j]);
                }
            }

        }

        public string[] setImageNotFound(string[] values) { 
            string imagen_ES = "";
            string imagen_EN = "";
            values[2] = "null";
            if ((string)this.InputMessage.input.str_language == "EN"){
                if (imagen_EN == ""){
                    using (WebClient client = new WebClient()){
                        imagen_EN = Convert.ToBase64String(client.DownloadData("https://drive.google.com/uc?export=view&id=10PDXbCGsTL5XCgVnQ9luyxbhCcTYjBO4")); 
                        //imagen_EN = "https://drive.google.com/uc?export=view&id=10PDXbCGsTL5XCgVnQ9luyxbhCcTYjBO4";   //borrar
                    }
                }                               
                values[3] = imagen_EN;
            }
            else{
                if (imagen_ES == ""){
                    using (WebClient client = new WebClient()){
                        imagen_ES = Convert.ToBase64String(client.DownloadData("https://drive.google.com/uc?export=view&id=1_5j3unXl2nxUxSVv8zPwonXYHDT2CAE5"));                   
                        //imagen_ES = "https://drive.google.com/uc?export=view&id=1_5j3unXl2nxUxSVv8zPwonXYHDT2CAE5";    //borrar                             
                    }
                }
                values[3] = imagen_ES;
            }
            return values;
        }
    }
}

