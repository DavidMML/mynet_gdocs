using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace create_google_doc.ReportsFactory.Reports
{
    public class IndexMeanByFieldReport : AbstractReport
    {
        private string[] _AllImgsInBase64;
        private string Language { get; set; }
        private readonly string _consoleFlag = "[3/4] Generating Index Mean by Field report. Please, wait...";
        private readonly string _gasFunction = "do_report_IndicesPromediosPorLote";

        public IndexMeanByFieldReport(dynamic message)
        {
            this.ConsoleFlag = _consoleFlag;
            this.GasFunction = _gasFunction;
            this.InputMessage = message;
            this.Language = (string)message.input.str_language;
        }

        public override async Task createReport(){
            this.SetInputJson();
            await this.fillImages(this.InputMessage.input.json__input.images);
            this.fillLotes(this.InputMessage.input.str_geojson_region.features);
            this.setBoxplotBase64();
            Console.WriteLine(" Done.");
        }

        public override void SetInputJson(){
            var jsonInput = this.InputMessage.input.json__input;
            this.InputJson = JsonConvert.DeserializeObject(
            "{   Farm : \""            + (string)jsonInput.Farm                   + "\"," +
                "InformDate : \""      +         jsonInput.Date                   + "\"," +
                "InformDataDate : \""  +         this.InputMessage.input.str_date + "\"," +
                "Season : \""          + (string)jsonInput.Season                 + "\"," +
                "Language : \""        + this.Language                            + "\"," +
                "Title : \""           + (string)jsonInput.Title                  + "\"," +
                "TotalHas : \""        + (string)jsonInput.TotalHas               + "\"," +
                "Images : {"           +
                   "MapaLotesYCultivos : \"\"," +
                   "Indexs: [] }," +
                "Lotes : [] ," +
                "BoxPlots : []" +
             "}"
            );
        }

        private async Task fillImages(dynamic images){
            await this.setImagesToString64(images);
            this.InputJson.Images.MapaLotesYCultivos = this._AllImgsInBase64[0];
            var indexs = this.InputMessage.input.json__input.images.Indexs;
            for (int i = 0; i < indexs.Count; i++){
                this.InputJson.Images.Indexs.Add(
                    JsonConvert.DeserializeObject(
                        "{" +
                            "type :  \""         + (string)indexs[i].type           + "\"," +
                            "image :  \""        + this._AllImgsInBase64[i * 2 + 1] + "\"," +
                            "averagedImage : \"" + this._AllImgsInBase64[i * 2 + 2] + "\"," +
                            "colorColumn : \""   + (string)indexs[i].colorColumn    + "\"," +
                            "date : \""          + indexs[i].date                   + "\", }"
                        )
                    );
            }
        }

        private async Task setImagesToString64(dynamic images){
            int imagesAmount = images.Indexs.Count * 2 + 1;
            List<string> AllImgUrls = new List<string>();
            this._AllImgsInBase64 = new string[imagesAmount];

            AllImgUrls.Add((string)images.MapaLotesYCultivos);
            for (int i = 0; i < images.Indexs.Count; i++){
                AllImgUrls.Add((string)images.Indexs[i].image);
                AllImgUrls.Add((string)images.Indexs[i].layeredImage);
            }

            this._AllImgsInBase64 = await ParallelizeArray(this._AllImgsInBase64, AllImgUrls);
        }

        private void fillLotes(dynamic lotes){
            for (int i = 0; i < lotes.Count; i++){
                var actualLote = lotes[i].properties;
                string crop = (string)actualLote.Crop != null ? (string)actualLote.Crop : (string)actualLote.crop;
                string cropId = (string)actualLote.cropselected;
                var color = this.getCssColor(cropId);
                var stringToFill =
                     "{" +
                        "FieldName : \"" + (string)actualLote.field_name + "\"," +
                        "Crop : \""      + crop                          + "\"," +
                        "CropId : \""    + cropId                        + "\"," +
                        "CropDate : \""  + actualLote.crop_date          + "\"," +
                        "HibVar : \""    + (string)actualLote.crop_hib   + "\"," +
                        "Color : \""     + color                         + "\"," +
                        "Has : "         + actualLote.area               + "," +
                        "Indexs : [] }";
                this.InputJson.Lotes.Add(JsonConvert.DeserializeObject(stringToFill));
                this.fillLotesIndexs(i, actualLote.indexs);
                // fill LotesIndexs
            }
        }

        private void fillLotesIndexs(int loteIndex, dynamic indexs){
            for (int j = 0; j < indexs.Count; j++){
                var type = (string)indexs[j].type;
                var stringToDeserialize = "{" +
                    "type :     \"" + type              + "\"," +
                    "mean :     \"" + indexs[j].mean    + "\"," +
                    "std :      \"" + indexs[j].std     + "\"," +
                    "min :      \"" + indexs[j].min     + "\"," +
                    "max :      \"" + indexs[j].max     + "\"," +
                    "firstQt :  \"" + indexs[j].firstQt + "\"," +
                    "median :   \"" + indexs[j].median  + "\"," +
                    "thirdQt :  \"" + indexs[j].thirdQt + "\"}";
                this.InputJson.Lotes[loteIndex].Indexs.Add(
                    JsonConvert.DeserializeObject(stringToDeserialize)
                );
            }
        }
        private void setBoxplotBase64(){
            for (int i = 0; i < this.InputJson.Images.Indexs.Count; i++){   
                var currentIndex = (string)this.InputJson.Images.Indexs[i].type;
                var dataToGenarateBoxPlot = getDataToGenarateBoxPlot(currentIndex);
                JsonConvert.SerializeObject(dataToGenarateBoxPlot);
                var imageName = currentIndex.ToUpper() + "_BoxPlot";
                this.InputJson.BoxPlots.Add(
                        JsonConvert.DeserializeObject(
                            "{ Index: \"" + currentIndex + "\"," + 
                              "BoxPlot: \"\"}"));
                this.InputJson.BoxPlots[i].BoxPlot = Rnode.Rnode.GetRChart(dataToGenarateBoxPlot, imageName, currentIndex);
            }
        }
        private dynamic getDataToGenarateBoxPlot(string index){
            dynamic dataToGenarateBoxPlot = JsonConvert.DeserializeObject(
            "{   Graphic : \"boxplot_index\"," +
                "Lotes : [] }"
            );
            for (int i = 0; i < this.InputJson.Lotes.Count; i++) {
                dataToGenarateBoxPlot.Lotes.Add(
                    JsonConvert.DeserializeObject(
                        "{" +
                            "name : \""    + this.InputJson.Lotes[i].FieldName + "\"," +
                            "cultivo : \"" + this.InputJson.Lotes[i].Crop      + "\"," +
                            "color : \""   + this.InputJson.Lotes[i].Color     + "\"," +
                            "distributionData : []" +
                        "}"
                    )
                );
                var myIndexs = this.InputJson.Lotes[i].Indexs;
                for (int j = 0; j < myIndexs.Count; j++) {
                    var indexType = ((string)myIndexs[j].type).ToLower();
                    if (indexType == index.ToLower()){
                        float[] vectorValues = new float[5];
                        vectorValues[0] = myIndexs[j].min;
                        vectorValues[1] = myIndexs[j].firstQt;
                        vectorValues[2] = myIndexs[j].median;
                        vectorValues[3] = myIndexs[j].thirdQt;
                        vectorValues[4] = myIndexs[j].max;
                        for (int k = 0; k < vectorValues.Length; k++) {
                            dataToGenarateBoxPlot.Lotes[i].distributionData.Add(
                                JsonConvert.DeserializeObject(
                                    vectorValues[k].ToString()
                                )
                            );
                        }
                    }
                }
            }
            return dataToGenarateBoxPlot;
        }
        
        public override void SetOutputJson() { }
    }
}

// Output = 
// {
//     "Language": "EN",                                                
//     "Title": "Renhardt bdy Peas F20",                                
//     "Season": "2018-19",                                             
//     "Farm": "McCain_Renhard Bdy LN Peas",                            
//     "InformDate": "2019-6-1",              
//     "TotalHas" : "163.73",                          
//     "Images": {                                                              
//         "MapaLotesYCultivos": "1XCiHjXsz2vd1J45t82bizcui-HXrG-5y",   
//         "Indexs": [                                                      
//             {  "type": "ndvi",                                      
//                 "image": "1ETiEWCVxKGKDP4J_vfdCuqMB2TpogtMp",        
//                 "averagedImage": "1qAHBRFQuLt8ir0hXoAMySrtTXCzn_AK7",
//                 "colorColumn": "#448809"                             
//             },
//             {   "type": "ndre"
//                 ...
//             },
//             {}
//         ]
//     },
//     "Lotes": [
//         {   "FieldName": "Lote 1",                            
//             "Crop": "Soybean",                                
//             "CropId": "2",                                    
//             "DatePlanting": "- No asignado -",                
//             "HibVar": "- No asignado -",                      
//             "Color": "#55AA00",                               
//             "crop_date": "2018-12-01T13:00:00Z",              
//             "Has": 60.52,
//             "Indexs": [
//                 {   "type": "ndvi",
//                     "mean": "0.3",
//                     "std": "0.15",
//                     "min": "0.2",
//                     "max": "0.9",
//                     "firstQt": "0.3", 
//                     "median": "0.4",
//                     "thirdQt": "0.72",
//                 },
//                 {   "type": "ndre",
//                     "mean": "0.25",
//                     "std": "0.07",
//                     "min": "0.05",
//                     "max": "0.56"
//                 },
//                 {}
//         },
//         {   "FieldName": "Lote 2",                            
//             "Crop": "Crop",                                
//             ...
//         },
//     ],
//     "BoxPlots" : [
//         {   Index: "ndvi",
//             BoxPlot: "iVBORw0KGgoAAAANSUhEUgAAAHgCAMb......."        
//         },    
//         {   Index: "ndre",
//             BoxPlot: "hEUgAAAeAABORw0KGgoAAAANSUhEUg......."        
//         },    
//         ...
//      ]
//     "NdviBoxPlot": "iVBORw0KGgoAAAANSUhEUgAAAeAAAAHgCAMA......."        
// }