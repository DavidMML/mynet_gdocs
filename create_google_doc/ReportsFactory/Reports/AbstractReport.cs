using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace create_google_doc.ReportsFactory.Reports
{
    public abstract class AbstractReport
    {
        public string GasFunction { get; set; } //function  name in google apps script
	    public string ConsoleFlag { get; set; } //[3/4]Generating {reportName}.\tPlease, wait...
        public dynamic InputMessage { get; set; } //json input from amazon queue
        public dynamic InputJson { get; set; } //generating 
        public dynamic OutputJson { get; set; } //generating output data, output data is from gee
        
        public abstract void SetInputJson();
        public abstract void SetOutputJson();
        public abstract Task createReport();

        public static async Task<string> DownloadData(string url)
        {
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
            HttpWebResponse resp = (HttpWebResponse) await req.GetResponseAsync();

            StreamReader sr = new StreamReader(resp.GetResponseStream());
            string results = await sr.ReadToEndAsync();
            sr.Close();

            return results;
        }

        static public async Task<string[,]> ParallelizeMatrix(string[,] AllLotsImgsInBase64, List<List<string>> AllLotsImgUrls)
        {
            ParallelOptions po = new ParallelOptions { MaxDegreeOfParallelism = 10 };
            await Task.Factory.StartNew(() =>
            {
                Parallel.ForEach(AllLotsImgUrls, po, (urlsPerLot, pls0, fieldIndex) =>
                {
                    Parallel.ForEach(AllLotsImgUrls[(int)fieldIndex], po, (url, pls, dateIndex) =>
                    {
                        if (url.StartsWith("http"))
                        {
                            //Console.WriteLine($"Descargando imágen [{(int)indiceLote}][{(int)indice}]...");
                            using (WebClient client = new WebClient())
                            {
                                AllLotsImgsInBase64[(int)fieldIndex, (int)dateIndex] = Convert.ToBase64String(client.DownloadData(url));
                                //AllLotsImgsInBase64[(int)fieldIndex, (int)dateIndex] = url; //borrar
                            }
                        }
                        else
                        {
                            //Console.WriteLine($"Copiando imágen estándar en [{(int)indiceLote}][{(int)indice}]...");
                            AllLotsImgsInBase64[(int)fieldIndex, (int)dateIndex] = url;
                        }
                    });
                });
            });
            return AllLotsImgsInBase64;
        }

        static public async Task<string[]> ParallelizeArray(string[] AllLotsImgsInBase64, List<string> AllLotsImgUrls)
        {
            ParallelOptions po = new ParallelOptions { MaxDegreeOfParallelism = 50 };
            await Task.Factory.StartNew(() =>
            {
                Parallel.ForEach(AllLotsImgUrls, po, (url, pls, i) =>
                {
                    if (url.StartsWith("http"))
                    {
                        //Console.WriteLine($"Descargando imágen [{(int)indice}]...");
                        using (WebClient client = new WebClient())
                        {
                            AllLotsImgsInBase64[(int)i] = Convert.ToBase64String(client.DownloadData(url));
                            //AllLotsImgsInBase64[(int)i] = url; //borrar
                        }
                    }
                    else
                    {
                        //Console.WriteLine($"Copiando imágen estándar en [{(int)indice}]...");
                        AllLotsImgsInBase64[(int)i] = url;
                    }
                });
            });
            return AllLotsImgsInBase64;
        }

        public string getCssColor(string cropId){
            // string[,] colorArray = new string[,]{
            string[,] colorArray = new string[,]{
                {"1","#DBDADA"},     //"no asignado"
                {"2","#55AA00"},     //"soja"
                {"21","#BBDD00"},    //"soja2°"
                {"3","#FF6600"},     //"maíz"
                {"22","#FF9800"},    //"maíz2°"
                {"4","#F3E689"},     //"trigo"
                {"5","#B9B246"},     //"cebada"
                {"6","#FFCC00"},     //"girasol"
                {"7","#C75833"},     //"lenteja"
                {"8","#AFF553"},     //"garbanzo"
                {"9","#D7C38B"},     //"arroz"
                {"10","#57BB8A"},    //"cañadeazuca"
                {"11","#861970"},    //"sorgo"
                {"12","#BF80FF"},    //"poroto"
                {"13","#045004"},    //"pasturas"
                {"14","#BDBDBD"},    //"barbecho"
                {"15","#6596C6"},    //"alfalfa"
                {"16","#FFFD26"},    //"colza"
                {"17","#C2D100"},    //"arveja"
                {"18","#C8D9A4"},    //"avena"
                {"19","#D37F67"},    //"mani"
                {"","#5D19C9"},      //"poroto" //otro poroto que no se usa
                {"20","#F5EBEB"},    //"algodon"
                {"49","#A05F1C"},    //"papa"
            };
            for (int i = 0; i < colorArray.GetLength(0); i++){
                if ( colorArray[i, 0] == cropId.ToString()) {
                    return colorArray[i, 1];
                }
            }
            return "color not found";
        }
    }
}