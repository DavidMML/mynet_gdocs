
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace create_google_doc.ReportsFactory.Reports
{
    public class CropScoutingReport : AbstractReport
    {
        private int _vistaCount;
        private string[] _AllImgsInBase64;
        private int _datosRecorridosCount;
        private readonly string _consoleFlag = $"[3/4] Generating Crop Scouting.\t\tPlease, wait...";
        private readonly string _gasFunction = "do_report_Recorridos";

        public CropScoutingReport(dynamic message){
            this.ConsoleFlag = _consoleFlag;
            this.GasFunction = _gasFunction;
            this.InputMessage = message;

            // this._datosRecorridosCount = this.InputMessage.input.json__input.DatosRecorridos.Count;
            this._vistaCount = this.InputMessage.input.vistaDetalle.Count;
        }

        public override async Task createReport(){
            await this.setImagesToString64();
            this.SetInputJson();
            var listFeatures = ((IEnumerable)this.InputMessage.input.points.features).Cast<dynamic>().ToList();
            this.SetDynamicTableAndDatosRecorridosDetalles(listFeatures);
            this.SetDatosRecorridosDetalleImages(listFeatures);
            this.SetVistas();
            Console.WriteLine(" Done.");
        }

        public async Task setImagesToString64(){
            // int imagesAmount = this._datosRecorridosCount + this._vistaCount + 1;
            int imagesAmount = this._vistaCount + 1;
            List<string> AllImgUrls = new List<string>();
            this._AllImgsInBase64 = new string[imagesAmount];

            // for (int i = 0; i < this._datosRecorridosCount; i++)
            //     AllImgUrls.Add((string)this.InputMessage.input.json__input.DatosRecorridos[i].image);

            AllImgUrls.Add((string)this.InputMessage.input.vista);
            for (int i = 0; i < this._vistaCount; i++)
                AllImgUrls.Add((string)this.InputMessage.input.vistaDetalle[i]);

            this._AllImgsInBase64 = await ParallelizeArray(this._AllImgsInBase64, AllImgUrls);
        }

        public override void SetInputJson(){
            string docName = "" + (string)this.InputMessage.input.json__input.Title + " - " + (string)this.InputMessage.input.json__input.Farm;
            var input = this.InputMessage.input;
            this.InputJson = JsonConvert.DeserializeObject(
            "{   language : \"" + (string)input.str_language + "\"," +
                "informDate : \"" + (string)input.str_date + "\"," +
                "season : \"" + (string)input.json__input.Season + "\"," +
                "farm : \"" + (string)input.json__input.Farm + "\"," +
                "imageDate : \"" + (string)input.json__input.ImageDate + "\"," +
                "position : \"" + (string)input.json__input.Position + "\"," +
                "docName : \"" + docName + "\"," +
                "name_fieldVisit : \"" + (string)input.json__input.Name_fieldVisit + "\"," +
                "dynamicTable: [] ," +
                "datosRecorridosDetalle : [] ," +
                "datosRecorridosDetalleImages : []," +
                "vista : \"\"," +
                "vistaDetalle : []}"
            );
        }
        private void SetDynamicTableAndDatosRecorridosDetalles(List<dynamic> listFeatures){
            var arrayToDynamicTable = new JArray();
            foreach (var i in listFeatures){
                var jObject = new JObject();
                foreach (JProperty j in i.properties){
                    var jName = j.Name.ToString();
                    if (!jName.StartsWith("photo") && !jName.StartsWith("meta"))
                        jObject.Add(new JProperty(j.Name, (string)j.Value));
                }
                arrayToDynamicTable.Add(jObject);
            }

            var titleBold = true;
            var aaa = JsonConvert.SerializeObject(arrayToDynamicTable);
            var vertical = ((string)this.InputJson.position).ToLower() == "vertical" ? true :  false;
            var DynamicTable = new DynamicTable(aaa, titleBold, "Muestra", vertical);

            this.InputJson.dynamicTable = DynamicTable.getDynamicTable().Tables;
            this.InputJson.datosRecorridosDetalle = JArray.FromObject(DynamicTable.getArrayData(true));
        }

        private void SetDatosRecorridosDetalleImages(List<dynamic> listFeatures){
            var listTodatosRecorridosDetalleImages = new JArray();
            foreach (var i in listFeatures){
                var jObject = new JObject();
                var photo = i.properties.photo?? " - ";
                var photo_id = i.properties.photo_id ?? " - ";
                jObject.Add(new JProperty("photo", (string)photo));
                jObject.Add(new JProperty("photo_id", (string)photo_id));
                listTodatosRecorridosDetalleImages.Add(jObject);
            }
            this.InputJson.datosRecorridosDetalleImages = listTodatosRecorridosDetalleImages;
        }

        private void SetVistas(){
            string vistaDetallesConcat = "[";
            for (int i = 0; i < _vistaCount; i++){
                vistaDetallesConcat += "\"" + this._AllImgsInBase64[this._datosRecorridosCount + i + 1] + "\"";
                vistaDetallesConcat += (i != _vistaCount - 1) ? "," : "";
            }

            this.InputJson.vista = this._AllImgsInBase64[this._datosRecorridosCount];
            this.InputJson.vistaDetalle = JsonConvert.DeserializeObject(vistaDetallesConcat += "]");
        }
        public override void SetOutputJson() { }
    }
}

//Salida esperada
// {
//  "InputJson": {
//     "language": "ES",
//     "informDate": "22-05-19",
//     "season": "2018-2019",
//     "farm": "La Margarita",
//     "imageDate": "1/3/2019",
//     "docName": "Recorridos - La Margarita",
//     "name_fieldVisit" : "LM2019abr23",
//     "datosRecorridosDetalle": [
//         ["Muestra", "Tipo de malezas", "Nombre de la especie", "Abundancia", "Tipo de malezas 2", "Nombre de la especie 2", "Abundancia 2", "Otras malezas", "Infestacion", "Observaciones"],
//         ["1", "Graminea", "Rama Oscura ", "1- plantas/m2", "Ninguna", "Ninguna", "0 plantas/m2", "", "Moderado", "Kill them!"],
//         ["2", "Graz", "Rama Negra (Conyza bonariensis)", "1-4 plantas/m2", "Ninguna", "Ninguna", "0 plantas/m2", "", "Moderado", "killed"],
//         ["3", "cel", "Rama  (Conyza )", "1-4 plantas/m2", "Ninguna", "Ninguna", "0 plantas/m2", "", "Complicado", "Malesa 3"]],
//     "datosRecorridosDetalleImages": [
//         {
//             "photo": "2019-06-28 13-20-48.jpg",
//             "photo_id": "1xhA1JiHLR2mfgysiu4jNg2kl1nylsp7c"
//         },
//         {
//             "photo": "Unknown 2019-06-28 13-20-48.jpg",
//             "photo_id": "1Jso2BsNrqEAt4R4Vb64dKnnqg9-1cNwA"
//         },
//         {
//             "photo": "Example 2019-06-28 13-20-48.jpg",
//             "photo_id": "14ve7qCj3_JyajueTJFhoBENh36PFfMxh"
//         }
//     ],
//     "dynamicTable": [
//         {
//             "Data": [
//                 ["Muestra", "Tipo de malezas", "Nombre de la especie", "Abundancia", "Tipo de malezas 2"],
//                 ["1", "Graminea", "Rama Oscura ", "1- plantas/m2", "Ninguna"],
//                 ["2", "Graz", "Rama Negra (Conyza bonariensis)", "1-4 plantas/m2", "Ninguna"],
//                 ["3", "cel", "Rama  (Conyza )", "1-4 plantas/m2", "Ninguna"]
//             ],
//             "Sizes": [54, 103, 103, 103, 103],
//             "TitleBold": true
//         },
//         {
//             "Data": [
//                 ["Nombre de la especie 2", "Abundancia 2", "Otras malezas", "Infestacion", "Observaciones"],
//                 ["Ninguna", "0 plantas/m2", "aaa", "Moderado", "Kill them!"],
//                 ["Ninguna", "0 plantas/m2", "bb", "Moderado", "killed"],
//                 ["Ninguna", "0 plantas/m2", "ccccccc", "Complicado", "Malesa 3"]
//             ],
//             "Sizes": [93, 93, 93, 93, 93],
//             "TitleBold": true
//         }
//     ],
//     "vista": "1UXw26qfJwJD14UxiziHYg47oYiUUvr2a",
//     "vistaDetalle": [
//         "1EOaHKXpNenKqvRBOwrCDt_hE41iW9mEW",
//         "1jIFo57lAh1ZPu1kYr5r5lDv_qx-iNv_E",
//     ]
//  }
//}