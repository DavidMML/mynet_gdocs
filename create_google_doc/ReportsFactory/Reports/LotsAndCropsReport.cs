using Newtonsoft.Json;
using System;
using System.Threading.Tasks;
namespace create_google_doc.ReportsFactory.Reports
{
    public class LotsAndCropsReport : AbstractReport
    {
        private string Language { get; set; }
        private readonly string _consoleFlag = "[3/4] Generating Lots and Crops report.\tPlease, wait...";
        private readonly string _gasFunction = "do_report_LotesYCultivos";

        public LotsAndCropsReport(dynamic message)
        {
            this.ConsoleFlag = _consoleFlag;
            this.GasFunction = _gasFunction;
            this.InputMessage = message;
            this.Language = (string)message.input.str_language;
        }

        public override async Task createReport(){
            this.SetInputJson();
            //this.SetOutputJson();
            Console.WriteLine(" Done.");
        }

        public override void SetInputJson(){
            string docName;
            if (this.Language == "ES") {
                docName = "Lotes y Cultivos - ";
            }else{
                docName = "Crops and Lots - ";
            }
            docName += (string)this.InputMessage.input.json__input.Farm;
           
            this.InputJson = JsonConvert.DeserializeObject( 
            "{   Farm : \""                  + (string)this.InputMessage.input.json__input.Farm      + "\"," + 
                "InformDate : \""            + (string)this.InputMessage.input.json__input.Date      + "\"," + 
                "Season : \""                + (string)this.InputMessage.input.json__input.Season    + "\"," + 
                "Language : \""              + this.Language                                         + "\"," +
                "Title : \""                 + docName                                               + "\"," +
                "sheetPosition : \""         + (string) this.InputMessage.input.json__input.Position + "\"," + 
                "image_lotesYCultivos : \""  + (string)this.InputMessage.input.image_lotesYCultivos  + "\"," + 
                "TotalHas : \""                                                                      + "\"," +
                "Cultivos: [] }"              
            );

            var cultivos = this.InputMessage.input.str_geojson_region.features;
            float totalHas = 0;
            for ( int i = 0; i < cultivos.Count; i++ ){
                totalHas += (float)cultivos[i].properties.area;
                string crop = (string) cultivos [i].properties.Crop != null ? (string) cultivos [i].properties.Crop : (string) cultivos [i].properties.crop;
                string cropId = (string)cultivos[i].properties.cropselected;
                var color = this.getCssColor(cropId);
                this.InputJson.Cultivos.Add (
                   JsonConvert.DeserializeObject (
                    "{" +
                        "FieldName : \"" + cultivos[i].properties.field_name + "\"," +
                        "Crop : \""      + crop + "\"," +
                        "CropId : \""    + cropId + "\"," +
                        "Has : "         + cultivos[i].properties.area + "," +
                        "Color : \""     + color + "\"" +
                    "}"));
            }
            this.InputJson.TotalHas = totalHas;
        }

        public override void SetOutputJson(){}
    }
}
