using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace create_google_doc.ReportsFactory.Reports
{
    public class ZonesReport : AbstractReport
    {
        private string _method = "";
        private int _amountClasses;

        private readonly string _consoleFlag = "[3/4] Generating Zones report.\t\tPlease, wait...";
        private readonly string _gasFunction = "do_report_ZonesNDVI";

        public ZonesReport(dynamic message){ 
            this.ConsoleFlag = _consoleFlag;
            this.GasFunction = _gasFunction;
            this.InputMessage = message;
        }

        public override async Task createReport(){
            this.SetInputJson();
            this.SetOutputJson();
            await this.SetOutputGeoJson();
            Console.WriteLine(" Done.");
        }

        public override void SetInputJson() {
            var Language = (string)this.InputMessage.input.str_language;
            this.InputJson = this.InputMessage.input.json__input;
            this.InputJson.Language = Language;

            // string newReportType = (string)this.InputMessage.input.str_function;
            string strFunction = (string)this.InputMessage.input.str_function;
            string[] splitedStrFunction = strFunction.Split("_");//                                  
            if (splitedStrFunction.Length == 4) { //"str_function": "zonificacion_ndvi_lista_s2",       Borrar cuando no se huse mas VIEJO!!!!!!
                this.InputJson.IndexType = "NDVI";
                SetMethodType(splitedStrFunction[2], Language);
            }else {                              //"str_function": "zonificacion_index_s2",
                var indexType = (string)this.InputMessage.input.json__input.Index;
                this.InputJson.IndexType = indexType.ToUpper( );
                var methodName = (string)this.InputMessage.input.json__input.Method;
                SetMethodType(methodName, Language);
            }
            // string method = ;
        }
        public void SetMethodType(string methodName, string Language) { 
            switch ( methodName ){
                case "cuantiles":
                    this._method = "quantiles";
                    this.InputJson.Method = (Language == "EN") ? "Quantiles" : "Cuantiles";
                    break;
                case "cuantil":
                    this._method = "quantiles";
                    this.InputJson.Method = (Language == "EN") ? "Quantiles" : "Cuantiles";
                    break;
                case "cluster":
                    this._method = "clusters";
                    this.InputJson.Method = (Language == "EN") ? "Clusters" : "Clústeres";
                    break;
                default:
                    this._method = "range";
                    this.InputJson.Method = (Language == "EN") ? "  Value range" : "Rango de valores";
                    break;
            }
            this._amountClasses = (this._method == "range") ? (this.InputMessage.input.json__input.cut_list.Count + 1) : (int)this.InputMessage.input.json__input.Classes;
        }

        public override void SetOutputJson() {  
            this.OutputJson = JsonConvert.DeserializeObject("{URLThumb1: \"\",URLThumb2: \"\",Classes: []}");
            this.OutputJson.URLThumb1 = this.InputMessage.input.url_index == null ?
                 (string)this.InputMessage.input.url_ndvi : 
                 (string)this.InputMessage.input.url_index;
            this.OutputJson.URLThumb2 = (string)this.InputMessage.input.url_clases;  

            string[] pallette;
            switch (this._amountClasses)
            {
                case 2: { pallette = new string[] { "#ed6e43", "#b3df76" }; break; }
                case 3: { pallette = new string[] { "#ed6e43", "#ffe8a5", "#b3df76" }; break; }
                case 4: { pallette = new string[] { "#d7191c", "#feba6f", "#e6f5a8", "#6abd58" }; break; }
                case 5: { pallette = new string[] { "#d7191c", "#ed6e43", "#ffe8a5", "#b3df76", "#6abd58" }; break; }
                case 6: { pallette = new string[] { "#d7191c", "#ed6e43", "#feba6f", "#e6f5a8", "#b3df76", "#6abd58" }; break; }
                case 7: { pallette = new string[] { "#d7191c", "#ed6e43", "#feba6f", "#ffe8a5", "#e6f5a8", "#b3df76", "#6abd58" }; break; }
                default:{ pallette = new string[] { "#d7191c", "#ed6e43", "#feba6f", "#ffe8a5", "#e6f5a8", "#b3df76", "#6abd58" }; break; }
            }

            for (int i = 0; i < this._amountClasses; ++i){
                this.OutputJson.Classes.Add(JsonConvert.DeserializeObject("{}"));
                this.OutputJson.Classes[i].name = i.ToString();
                this.OutputJson.Classes[i].color = pallette[i];
                this.OutputJson.Classes[i].area_has = 0.0M;
                this.OutputJson.Classes[i].percent = 0.0M;
                this.OutputJson.Classes[i].max = 0.0M;
                this.OutputJson.Classes[i].mean = 0.0M;
                this.OutputJson.Classes[i].min = 0.0M;
                this.OutputJson.Classes[i].productiv = i;
            }
        }

        private async Task SetOutputGeoJson(){ 
            dynamic geojson = JsonConvert.DeserializeObject(await DownloadData((string)this.InputMessage.input.url_geojson));
            int clase = 0;
            decimal totalArea = 0.0M;
            List<int> clasesCount = new List<int>(new int[this._amountClasses]);

            foreach (dynamic feature in geojson.features){
                clase = (int)feature.properties.productiv;

                this.OutputJson.Classes[clase].name       =       (string)feature.properties.productiv;
                this.OutputJson.Classes[clase].color      = "#" + (string)feature.properties.color;
                this.OutputJson.Classes[clase].productiv  =          (int)feature.properties.productiv;
                this.OutputJson.Classes[clase].area_has  +=      (decimal)feature.properties.area_has;
                this.OutputJson.Classes[clase].max       +=      (decimal)feature.properties.NDVIMax;
                this.OutputJson.Classes[clase].mean      +=      (decimal)feature.properties.NDVI_mean;
                this.OutputJson.Classes[clase].min       +=      (decimal)feature.properties.NDVIMin;

                totalArea += (decimal)feature.properties.area_has;
                clasesCount[clase] += 1;
            }

            for (int i = 0; i < this._amountClasses; ++i){
                if (clasesCount[i] != 0){
                    this.OutputJson.Classes[i].percent = this.OutputJson.Classes[i].area_has * 100 / totalArea;
                    this.OutputJson.Classes[i].max = this.OutputJson.Classes[i].max / clasesCount[i];
                    this.OutputJson.Classes[i].mean = this.OutputJson.Classes[i].mean / clasesCount[i];
                    this.OutputJson.Classes[i].min = this.OutputJson.Classes[i].min / clasesCount[i];
                }
            }

        }
    }
}