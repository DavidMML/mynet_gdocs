using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using System.Linq;
using create_google_doc.ReportsFactory;

namespace create_google_doc.ReportsFactory
{
    public class DynamicTable {
        public PixelParserSingleton myPixelParser { get; set; } = PixelParserSingleton.Instance;
        private bool titleBold;
        private bool vertical;
        private ColumnTable ColumnIndex { get; set; }
        private List<ColumnTable> ColumnList { get; set; } = new List<ColumnTable>();
        private List<int> columnPixelsAcumulated = new List<int>();

        public DynamicTable(string inputMsg, bool titleBold, string indexColumnName, bool vertical) {
            this.setColumnListAndColumnIndex(inputMsg, indexColumnName);
            this.vertical = vertical;
            this.titleBold = titleBold;
        }

        private void setColumnListAndColumnIndex(string inputMsg, string indexColumnName){
            dynamic message = JsonConvert.DeserializeObject(inputMsg);
            for (int i = 0; i < message.Count; i++){                
                var indexProp = 0;
                foreach (JProperty j in message[i]){
                    var isIndexColumn = j.Name.ToLower() == indexColumnName.ToLower();
                    if (i == 0){ 
                        var numeric = double.TryParse((string)j.Value, out double n);
                        var newColumTable = new ColumnTable(){
                            Title = j.Name,
                            isNumeric = numeric,
                            Widht = numeric ? myPixelParser.getPixelsForLargeWord(j.Name, true) : 0 
                        };
                        if (isIndexColumn == true) 
                            this.ColumnIndex = newColumTable;
                        else 
                            this.ColumnList.Add(newColumTable);
                    }
                    if (isIndexColumn == true) { 
                        this.ColumnIndex.Values.Add((string)j.Value);
                    }else{ 
                        this.ColumnList[indexProp].Values.Add((string)j.Value);
                        indexProp++;
                    }
                }
            }
        }
        public dynamic getDynamicTable() {
            var indexCut = setAmountTables(this.ColumnList);
            var arrayToReturn = this.setOutputTableList(indexCut);
            arrayToReturn = completeRemaningSizes(arrayToReturn);
            var myReturn = setDynamicOutput(arrayToReturn, this.titleBold);
            return myReturn;
        }

        public String[,] getArrayData(bool withColumnIndex = false) { // put all data in two dinemcional array (incliding column titles)
            String[,] outputData;
            if(withColumnIndex ==  false){
                outputData = new String[this.ColumnList[0].Values.Count + 1, this.ColumnList.Count];
                for (int j = 0; j < this.ColumnList[0].Values.Count + 1; j++)
                    for (int i = 0; i < this.ColumnList.Count; i++)
                        outputData[j,i] = j == 0 ? this.ColumnList[i].Title : this.ColumnList[i].Values[j-1];
            } else { 
                outputData = new String[this.ColumnList[0].Values.Count + 1, this.ColumnList.Count + 1];
                for (int j = 0; j < this.ColumnList[0].Values.Count + 1; j++)
                    for (int i = 0; i < this.ColumnList.Count + 1; i++) {
                        if (i == 0) 
                            outputData[j, 0] = j == 0 ? this.ColumnIndex.Title : this.ColumnIndex.Values[j - 1];
                        else 
                            outputData[j,i] = j == 0 ? this.ColumnList[i-1].Title : this.ColumnList[i-1].Values[j-1];    
                    }
            }
            return outputData;
        }

        private List<int> setAmountTables(List<ColumnTable> data) { // set tables amount by pixels columns 
            var amountColumns = data.Count();
            var totalPixels = data.Select(x => x.Widht == 0 ? myPixelParser.minWidhtColumnString : x.Widht).Sum();
            int amountTables = totalPixels / (myPixelParser.getWidhtTable(this.vertical) - ColumnIndex.Widht) + 1;
            int pixelsByTable = totalPixels / amountTables + 1;
            var indexTable = 1;
            var indexCut = new List<int>();
            var myColumnPixelsAcumulated = new List<int>();
            var acumulator = ColumnIndex.Widht;
            for (var i = 0; i < data.Count - 1; i++){// o  -2
                var currentPixelPosition = pixelsByTable * indexTable;
                var acumulatorNextVal = acumulator + (data[i + 1].Widht == 0 ? myPixelParser.minWidhtColumnString : data[i + 1].Widht);
                if (currentPixelPosition > acumulator && currentPixelPosition <= acumulatorNextVal) { 
                    var currentIndexCut = (currentPixelPosition - acumulator) < (acumulatorNextVal  - currentPixelPosition)? i : i + 1;
                    indexCut.Add(currentIndexCut);
                    indexTable++;
                }
                acumulator += data[i].Widht == 0 ? myPixelParser.minWidhtColumnString : data[i].Widht ;
            }
            indexCut.Add(amountColumns - 1);
            return indexCut;
        }

        private List<OutputTable> setOutputTableList(List<int> indexCut) {
            var data = this.getArrayData();
            var sizes = this.ColumnList.Select(x => x.Widht).ToList();
            List<OutputTable> result = new List<OutputTable>();
            for (int k = 0; k < indexCut.Count; k++){
                OutputTable outputTable = new OutputTable();
                var from = k == 0 ? 0 : indexCut[k-1] + 1;
                var to  =  k == indexCut.Count ? data.GetLength(1) - 1 : indexCut[k];
                for (int i = 0; i < data.GetLength(0); i++){
                    List<string> row = new List<string>();
                    for (int j = from; j <= to; j++){
                        if (j == from) {
                            if (i == 0) { 
                                row.Add(ColumnIndex.Title);    
                                outputTable.Sizes.Add(ColumnIndex.Widht);
                            }else { 
                                row.Add(ColumnIndex.Values[i-1]);    //ver en segunda iteracion
                            }
                        }
                        row.Add(data[i, j]);
                        if(i==0) {
                            outputTable.Sizes.Add(sizes[j]);
                        }
                    }
                    outputTable.Data.Add(row);
                }
                result.Add(outputTable);
            }
            return result;
        }

        private List<OutputTable> completeRemaningSizes(List<OutputTable> outputTable){
            var columnsByTableWithoutSize = outputTable.Select(y => y.Sizes.Select(z => z).Where(x => x == 0).Count()).ToList();
            var pixelsRemaningByTable = outputTable.Select(y => myPixelParser.getWidhtTable(this.vertical) - y.Sizes.Select(z => z).Where(x => x != 0).Sum()).ToList();
            var sizes = new List<int>();
            for (int i = 0; i < columnsByTableWithoutSize.Count; i++) 
                sizes.Add(pixelsRemaningByTable[i]/columnsByTableWithoutSize[i]);
            for (int i = 0; i < outputTable.Count; i++) {
                for (int j = 0; j < outputTable[i].Sizes.Count; j++){
                    if (outputTable[i].Sizes[j] == 0) outputTable[i].Sizes[j] = sizes[i];
                }
            }
            return outputTable;
        }

        private dynamic setDynamicOutput(List<OutputTable> data, bool titleBold) {
            dynamic output = JsonConvert.DeserializeObject("{ Tables: []}");
            foreach (var i in data){
                dynamic table = JsonConvert.DeserializeObject(
                       "{" +
                           "Data : []," +
                           "Sizes : []," +
                           "TitleBold : \"" + titleBold.ToString() + "\"" +
                        "}"
                   );
                table.Sizes = new JArray(i.Sizes.Select(y => y));
                table.Data  = new JArray(i.Data.Select(x => new JArray(x.Select(y => y))));
                table.TitleBold = titleBold;
                output.Tables.Add(table);
            }
            return output;
        }

    }
}
//Output example: --------------- getDynamicTable() --------------- 
// "Tables": [
//     {   "Title": "Datos Recorridas - Síntesis",
//         "Data": [
//             ["Tipo de malezas 2", "Nombre de la especie 2", "Abundancia 2", "Otras malezas"],
//             ["Ninguna", "Ninguna", "0 plantas/m2", "aaa"],
//             ["Ninguna", "Ninguna", "0 plantas/m2", "no identificadas"]],
//         "Sizes": [117, 90, 117, 117],
//         "TitleBold": "True" },
//     {   "Title": "Datos Recorridas - Síntesis",
//         "Data": [
//             ["Muestra", "Tipo de malezas", "Nombre de la especie", "Rinde estimado (ton/ha)", "Abundancia"],
//             ["3", "Graminea", "Rama Negra (Conyza bonariensis)", "24", "1-4 plantas/m2"],
//             ["2", "yuyito primavera", "Señora porqueria (yuyinus yuyineis)", "5", "2-9 plantas/m2"]],
//         "Sizes": [47, 122, 122, 56, 122],
//         "TitleBold": "True" },
//     {   "Title": "Datos Recorridas - Síntesis",
//         "Data": [
//             ["Infestacion", "Observaciones", "Fertilizante", "toxicidad", "another data"],
//             ["Moderado", "Se encontraron animales mutilados", "Zafiro", "155", "1000"],
//             ["Alarmante", " - ", "Agristart", "328", "4000"]],
//         "Sizes": [122, 122, 122, 56, 47],
//         "TitleBold": "True"  }
// ]
//Output example: --------------- getArrayData() --------------- 
// [
//     ["Muestra","Tipo de malezas" ,"Nombre de la especie"               ,"Rinde estimado (ton/ha)","Abundancia"    ,"Tipo de malezas 2","Nombre de la especie 2","Abundancia 2","Otras malezas"   ,"Infestacion","Observaciones"                    ,"Fertilisante","toxicidad","another data"],
//     ["3"      ,"Graminea"        ,"Rama Negra (Conyza bonariensis)"    ,"24"                     ,"1-4 plantas/m2","Ninguna"          ,"Ninguna"               ,"0 plantas/m2",""                ,"Moderado"   ,"Se encontraron animales mutilados","Zafiro"      ,"155"      ,"1000"        ],
//     ["2"      ,"yuyito primavera","Señora porqueria (yuyinus yuyineis)", "5"                     ,"2-9 plantas/m2","Ninguna"          , "Ninguna"              ,"0 plantas/m2","no identificadas","Alarmante"  ," - "                              , "Agristart"  ,"328"      ,"4000"        ]
// ]