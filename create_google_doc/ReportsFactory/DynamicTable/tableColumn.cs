using System;
using System.Collections.Generic;

namespace create_google_doc.ReportsFactory
{
    public class ColumnTable{
        public String Title { get; set; }
        public bool isNumeric { get; set; } = false;
        public List<string> Values { get; set; } = new List<string>();
        public int Widht { get; set; }
        // public int MeanChartData { get; set; }
    }
}