using System.Collections.Generic;

namespace create_google_doc.ReportsFactory
{
    public class OutputTable
    {
        // public string Title { get; set; }
        public List<List<string>>  Data { get; set; } = new List<List<string>>();
        public List<int> Sizes { get; set; } = new List<int>();
        public bool titleBold { get; set; }
    }
}
//----------------Example-------------------------------------------------
//"Title": "Datos Recorridas - Síntesis",
//"Data": [
//    ["Tipo de malezas 2", "Nombre de la especie 2", "Abundancia 2", "Otras malezas"],
//    ["Ninguna", "Ninguna", "0 plantas/m2", "aaa"],
//    ["Ninguna", "Ninguna", "0 plantas/m2", "no identificadas"]],
//"Sizes": [117, 90, 117, 117],
//"TitleBold": "True" },