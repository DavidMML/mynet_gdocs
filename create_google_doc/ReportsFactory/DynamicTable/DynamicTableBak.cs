// using System;
// using Newtonsoft.Json;
// using System.Collections.Generic;
// using Newtonsoft.Json.Linq;
// using System.Linq;
// using create_google_doc.ReportsFactory;

// namespace create_google_doc.ReportsFactory
// {
//     public class DynamicTable {
//         private bool titleBold;
//         private List<ColumnTable> columnList { get; set; } = new List<ColumnTable>();
//         private List<int> columnPixelsAcumulated = new List<int>();

//         public DynamicTable(string inputMsg, bool titleBold, string indexColumn) {
//             this.setColumnList(inputMsg);
//             if (indexColumn == null) { 
//                 this.setColumnListSizes(titleBold);
//                 this.titleBold = titleBold;
//             }
//         }
//         public DynamicTable(string inputMsg, bool titleBold) {
//             this.setColumnList(inputMsg);
//             this.setColumnListSizes(titleBold);
//             this.titleBold = titleBold;
//         }

//         public dynamic getDynamicTable() {
//             var indexCut = setAmountTables(this.columnList);
//             var arrayToReturn = this.setOutputTableList(indexCut);
//             arrayToReturn = completeRemaningSizes(arrayToReturn);
//             return setDynamicOutput(arrayToReturn, this.titleBold);
//         }

//         public String[,] getArrayData() { // put all data in two dinemcional array (incliding column titles)
//             String[,] outputData = new String[this.columnList[0].Values.Count + 1, this.columnList.Count];
//             for (int j = 0; j < this.columnList[0].Values.Count + 1; j++)
//                 for (int i = 0; i < this.columnList.Count; i++)
//                     outputData[j,i] = j == 0 ? this.columnList[i].Title : this.columnList[i].Values[j-1];
//             return outputData;
//         }
        
//         private void setColumnList(string inputMsg){
//             dynamic message = JsonConvert.DeserializeObject(inputMsg);
//             for (int i = 0; i < message.Count; i++){                
//                 var indexProp = 0;
//                 foreach (JProperty j in message[i]){        
//                     if (i == 0) 
//                         columnList.Add(new ColumnTable(){
//                             Title = j.Name,
//                             isNumeric = double.TryParse((string)j.Value, out double n)
//                         });
//                     columnList[indexProp].Values.Add((string)j.Value);
//                     indexProp++;
//                 }
//             }
//         }
        
//         private void setColumnListSizes(bool titleBold){
//             var acumulator = 0;
//             for (var i = 0; i < columnList.Count; i++){
//                 columnList[i].Widht = columnList[i].isNumeric == false ? 0 :
//                     PixelParserSingleton.Instance.getPixelsForLargeWord(columnList[i].Title, titleBold);
//                 columnPixelsAcumulated.Add(acumulator += columnList[i].Widht == 0 ? PixelParserSingleton.minWidhtColumnString : columnList[i].Widht);
//             }
//         }    
        
//         private List<int> setAmountTables(List<ColumnTable> data) { // set tables amount by pixels columns
//             var amountColumns = data.Count();
//             var totalPixels = data.Select(x => x.Widht == 0 ? PixelParserSingleton.minWidhtColumnString : x.Widht).Sum();
//             int amountTables = totalPixels / PixelParserSingleton.widhtTable + 1;
//             int pixelsByTable = totalPixels / amountTables + 2;
//             var indexTable = 1;
//             var indexCut = new List<int>();
//             for (var i = 0; i < columnPixelsAcumulated.Count-1; i++){
//                 var currentPixelPosition = pixelsByTable * indexTable;
//                 if (currentPixelPosition > columnPixelsAcumulated[i] && currentPixelPosition <= columnPixelsAcumulated[i+1]) { 
//                     var currentIndexCut = (currentPixelPosition - columnPixelsAcumulated[i]) < (columnPixelsAcumulated[i + 1] - currentPixelPosition)? i : i+1;
//                     indexCut.Add(currentIndexCut);
//                     indexTable++;
//                 }
//             }
//             indexCut.Add(amountColumns - 1);
//             return indexCut;
//         }
        
//         private List<OutputTable> setOutputTableList(List<int> indexCut) {
//             var data = this.getArrayData();
//             var sizes = this.columnList.Select(x => x.Widht).ToArray();
//             List<OutputTable> result = new List<OutputTable>();
//             for (int k = 0; k < indexCut.Count; k++){
//                 OutputTable outputTable = new OutputTable();
//                 var from = k == 0 ? 0 : indexCut[k-1] + 1;
//                 var to  =  k == indexCut.Count ? data.GetLength(1) - 1 : indexCut[k];
//                 for (int i = 0; i < data.GetLength(0); i++){
//                     List<string> row = new List<string>();
//                     for (int j = from; j <= to; j++){
//                         row.Add(data[i, j]);
//                         if(i==0) outputTable.Sizes.Add(sizes[j]);
//                     }
//                     outputTable.Data.Add(row);
//                 }
//                 result.Add(outputTable);
//             }
//             return result;
//         }

//         private List<OutputTable> completeRemaningSizes(List<OutputTable> outputTable){
//             var columnsByTableWithoutSize = outputTable.Select(y => y.Sizes.Select(z => z).Where(x => x == 0).Count()).ToList();
//             var pixelsRemaningByTable = outputTable.Select(y => 468 - y.Sizes.Select(z => z).Where(x => x != 0).Sum()).ToList();
//             var sizes = new List<int>();
//             for (int i = 0; i < columnsByTableWithoutSize.Count; i++) 
//                 sizes.Add(pixelsRemaningByTable[i]/columnsByTableWithoutSize[i]);
//             for (int i = 0; i < outputTable.Count; i++) {
//                 for (int j = 0; j < outputTable[i].Sizes.Count; j++){
//                     if (outputTable[i].Sizes[j] == 0) outputTable[i].Sizes[j] = sizes[i];
//                 }
//             }
//             return outputTable;
//         }

//         private dynamic setDynamicOutput(List<OutputTable> data, bool titleBold) {
//             dynamic output = JsonConvert.DeserializeObject("{ Tables: []}");
//             foreach (var i in data){
//                 dynamic table = JsonConvert.DeserializeObject(
//                        "{" +
//                            "Data : []," +
//                            "Sizes : []," +
//                            "TitleBold : \"" + titleBold.ToString() + "\"" +
//                         "}"
//                    );
//                 table.Sizes = new JArray(i.Sizes.Select(y => y));
//                 table.Data  = new JArray(i.Data.Select(x => new JArray(x.Select(y => y))));
//                 table.TitleBold = titleBold;
//                 output.Tables.Add(table);
//             }
//             return output;
//         }

//     }
// }
// //Output example: --------------- getDynamicTable() --------------- 
// // "Tables": [
// //     {   "Title": "Datos Recorridas - Síntesis",
// //         "Data": [
// //             ["Tipo de malezas 2", "Nombre de la especie 2", "Abundancia 2", "Otras malezas"],
// //             ["Ninguna", "Ninguna", "0 plantas/m2", "aaa"],
// //             ["Ninguna", "Ninguna", "0 plantas/m2", "no identificadas"]],
// //         "Sizes": [117, 90, 117, 117],
// //         "TitleBold": "True" },
// //     {   "Title": "Datos Recorridas - Síntesis",
// //         "Data": [
// //             ["Muestra", "Tipo de malezas", "Nombre de la especie", "Rinde estimado (ton/ha)", "Abundancia"],
// //             ["3", "Graminea", "Rama Negra (Conyza bonariensis)", "24", "1-4 plantas/m2"],
// //             ["2", "yuyito primavera", "Señora porqueria (yuyinus yuyineis)", "5", "2-9 plantas/m2"]],
// //         "Sizes": [47, 122, 122, 56, 122],
// //         "TitleBold": "True" },
// //     {   "Title": "Datos Recorridas - Síntesis",
// //         "Data": [
// //             ["Infestacion", "Observaciones", "Fertilizante", "toxicidad", "another data"],
// //             ["Moderado", "Se encontraron animales mutilados", "Zafiro", "155", "1000"],
// //             ["Alarmante", " - ", "Agristart", "328", "4000"]],
// //         "Sizes": [122, 122, 122, 56, 47],
// //         "TitleBold": "True"  }
// // ]
// //Output example: --------------- getArrayData() --------------- 
// // [
// //     ["Muestra","Tipo de malezas" ,"Nombre de la especie"               ,"Rinde estimado (ton/ha)","Abundancia"    ,"Tipo de malezas 2","Nombre de la especie 2","Abundancia 2","Otras malezas"   ,"Infestacion","Observaciones"                    ,"Fertilisante","toxicidad","another data"],
// //     ["3"      ,"Graminea"        ,"Rama Negra (Conyza bonariensis)"    ,"24"                     ,"1-4 plantas/m2","Ninguna"          ,"Ninguna"               ,"0 plantas/m2",""                ,"Moderado"   ,"Se encontraron animales mutilados","Zafiro"      ,"155"      ,"1000"        ],
// //     ["2"      ,"yuyito primavera","Señora porqueria (yuyinus yuyineis)", "5"                     ,"2-9 plantas/m2","Ninguna"          , "Ninguna"              ,"0 plantas/m2","no identificadas","Alarmante"  ," - "                              , "Agristart"  ,"328"      ,"4000"        ]
// // ]