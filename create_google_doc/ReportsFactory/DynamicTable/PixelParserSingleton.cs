using System.Linq;
using System.Collections.Generic;
using System;

namespace create_google_doc.ReportsFactory
{
    public class PixelParserSingleton{
        private readonly Dictionary<string, double> pixelByLetterNotBold;
        private readonly Dictionary<string, double> pixelByLetterBold;

        private readonly string[] pixelByLetterRef;
        
        public readonly int minWidhtColumnString = 93;//px 93 for common strings,
        // public readonly int minWidhtColumnStringLarge = 125;//px for common strings, 
        public readonly int widhtTableVertical = 468; //px
        public readonly int widhtTableHorizontal = 654; //px
        private readonly int columnPadding = 6;//px


        private static PixelParserSingleton instance = new PixelParserSingleton();
        public static PixelParserSingleton Instance => instance;

        public int getWidhtTable(bool vertical = true) { 
            if (vertical == true)
                return this.widhtTableVertical;
            else {
                return this.widhtTableHorizontal;
            }
        }

        public PixelParserSingleton(){
            pixelByLetterRef = new string[]{ "il", "j", "ftr", "mw"};
            // values for Font: Arial, Size: 11, Not bold 
            pixelByLetterNotBold = new Dictionary<string, double>{
                {"il"   ,3},
                {"j"    ,4},
                {"mw"   ,9},
                {"ftr"  ,4},
                {"other",7.5}
            };
            // values for Font: Arial, Size: 11, BOLD 
            pixelByLetterBold = new Dictionary<string, double>{
                {"il"   ,3},
                {"j"    ,4},
                {"mw"   ,9},
                {"ftr"  ,4},
                {"other",8}
            };
        }

        private double getPixelByLetter(char letter, bool bold){
            var pixels = "other"; //g,h,a,b,c,d,e,k,n,o,p,q,s,u,v,x,y,z, ,1,2,3,4,5,6,7,8,9,0,
            foreach (var i in pixelByLetterRef)
                if (i.Contains(letter)) pixels = i;
            return bold == true ? pixelByLetterBold[pixels] : pixelByLetterNotBold[pixels];
        }

        public int getPixelsForLargeWord(string sentence, bool bold){ // gets the largest word on sentences and return his pixels
            var words = sentence.Split(" ").ToList();
            var largeWord = words.Select(x => x)
                                    .Where(x => x.Length == words.Select(y => y.Length).Max())
                                    .First();
            var largeWordPixels = largeWord.ToList().Select(x => getPixelByLetter(x, bold)).Sum();
            return (Convert.ToInt32(largeWordPixels) + columnPadding);  
        }

        public int getPixelsByTitle(string data, bool bold){ // return the pixels for the whole title
            return Convert.ToInt32(data.ToList().Select(x => getPixelByLetter(x, bold)).Sum());
        }
    }
}