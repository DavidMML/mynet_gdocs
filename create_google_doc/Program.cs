﻿using Amazon.SQS.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Microsoft.CSharp.RuntimeBinder;
using System;
using System.Threading;
using System.Threading.Tasks;
using System.IO;

namespace create_google_doc
{
    class GoogleDocsReport
    {
        private static string inputMsg = null;
        private SQSQueue sqsQueue;
        private string mySqsQueueUrl = "";
        private string service_name = "";

        public async Task CreateIfNotExistAndUse( string queueName ){
            // Instance of SQS technology.
            sqsQueue = new SQSQueue ( );

            // Creation of GoogleDocsReport input queues (if they do not exist).
            await sqsQueue.CreateQueueIfNotExists (queueName);

            // Selecting the queue. Now it's ready to be read from.
            mySqsQueueUrl = await sqsQueue.Use (queueName);

            service_name = queueName;

            return;
        }

        public async Task CleanQueue(){
            // Clean queue. For debugging purposes.
            await sqsQueue.CleanQueue ( );

            return;
        }

        public static void setTestMessage(){
            string [] arrayPath = AppDomain.CurrentDomain.BaseDirectory.Split ('\\');
            string finalPath = "";
            for ( int i = arrayPath.Length - 4; i < arrayPath.Length; i++ ) finalPath += @"\" + arrayPath [i];
            var pathWorkspace = AppDomain.CurrentDomain.BaseDirectory.Replace (finalPath, "") + @"\";
            //string jsonTestPath = pathWorkspace + "./Test/seriesThumbNdvi.json");                      //Andando
            string jsonTestPath = pathWorkspace + "./Test/changeRate.json";                      
            //string jsonTestPath = pathWorkspace + "./Test/zonesNdviCuantilesOLDStrFunction.json";      //Andando
            //string jsonTestPath = pathWorkspace + "./Test/zonesNdviCuantilesNEWStrFunction.json";      //Andando
            //string jsonTestPath = pathWorkspace + "./Test/zonesNdviClustersOLDStrFunction.json";       //Andando
            //string jsonTestPath = pathWorkspace + "./Test/zonesNdviClustersNEWStrFunction.json";       //Andando
            //string jsonTestPath = pathWorkspace + "./Test/zonesNdviRangeOLDStrFunction.json";          //Andando
            //string jsonTestPath = pathWorkspace + "./Test/zonesNdviRangeNEWStrFunction.json";          //Andando
            //string jsonTestPath = pathWorkspace + "./Test/lotsAndCrops.json";                          //Andando
            // string jsonTestPath = pathWorkspace + "./Test/indexMeanByField.json";
            //string jsonTestPath = pathWorkspace + @"Test\zonesNdviRangeNEWStrFunction.json";
            //string jsonTestPath = pathWorkspace + @"Test\BorrarZonesNdviOldTesting.json";
            // string jsonTestPath = pathWorkspace + @"./Test/recorridos.json";
            using ( StreamReader r = new StreamReader (jsonTestPath) ){
                inputMsg = r.ReadToEnd ( );
            }
        }

        public async Task<string> PopOneMessageFromSQS(string queueListened){
            // Getting test message.            
            Console.Write ("   Reading from " + queueListened + " SQS...");
            ReceiveMessageResponse msg = await sqsQueue.GetMessage (visibilityTimeout: 10);
            string message = msg.Messages [0].Body;
            Console.Write ("\tMessage found.");

            // Erasing the message form the queue.
            await sqsQueue.DeleteMessage (msg.Messages [0].ReceiptHandle);
            Console.WriteLine ("\tMessage consumed.");

            return message;
        }

        public async Task SendToSQS( dynamic message, string status, string result, int steps ){
            // Preparing message to send.
            var strfunction = (string) message.input.str_function;

            string url_geojson = (string) message.input.url_geojson;
            message.output = JsonConvert.DeserializeObject ("{}");
            message.output.status = status;
            if ( status == "error" )
                message.output.error = result;
            else{
                message.output.str_report_url = result;
                if ( strfunction.StartsWith ("zonificacion") )
                    message.output.url_geojson = url_geojson;
            }
            string orchestratorQueue = (string) message.input.str_output_queue;

            // Casting message to String.
            string messageStr = JsonConvert.SerializeObject (message, Formatting.Indented);

            // Send message to orchestrator.
            #if DEBUG == false
                Console.Write ($"[{steps}/{steps}] Sending {status} message to Orch's queue...");
                await sqsQueue.SendMessage (messageStr, orchestratorQueue);
                Console.WriteLine ("\tDone.");
            #endif

            return;
        }

        delegate JObject GetValueDelegate();
        private bool IsProperty( GetValueDelegate getValueMethod ){
            try{
                var v = getValueMethod ( );
                return (v == null) ? false : true;
            }
            catch ( RuntimeBinderException ) { return false; }
            catch { return true; }
        }

        public async Task OpenExecuteAndSend( string msg, bool debug ){
            dynamic message = JsonConvert.DeserializeObject (msg);
            string messag = "";

            int steps = 4;
            if ( ((string) message.input.str_function).StartsWith ("serie_") )
                steps = 5;

            Console.Write ($"\n[1/{steps}] Checking attributes in message.\tPlease, wait...");
            if ( IsProperty (() => message.input.google_email) &&
                IsProperty (() => message.input.google_client_id) &&
                IsProperty (() => message.input.google_client_secret) &&
                IsProperty (() => message.input.google_user_id) &&
                IsProperty (() => message.input.google_refresh_token) &&
                IsProperty (() => message.input.str_output_queue) )
            {
                Console.WriteLine (" Done.");

                try{
                    // Generating report.
                    //string URLOfGeneratedReport = await Test.CreateReport(message);
                    string googleAppsScript = "Development";//"Production", "Development", "Development2", 
                    string URLOfGeneratedReport = await ReportsFactory.ReportsFactory.Instance.CreateReport(message, googleAppsScript);
                    await SendToSQS (message, "success", URLOfGeneratedReport, steps);
                }
                catch ( Exception e ){
                    if ( e.InnerException != null )
                        messag = e.InnerException.Message;
                    else{
                        if ( e.Message.IndexOf ("Not Found") > -1 )
                            messag = "Algunos de los recursos necesarios para el reporte ya no están disponibles (el mensaje tardó mucho en cola).";
                        else
                            messag = e.Message;
                    }

                    Console.WriteLine ("");
                    await SendToSQS (message, "error", messag, steps);
                    if ( debug )
                        throw new Exception (messag);
                    else
                        throw new NullReferenceException ( );
                }
            }
            else{
                messag = "Uno o mas de un atributo de credencial no se encuentra en el mensaje.";

                Console.WriteLine ("");
                await SendToSQS (message, "error", messag, steps);
                if ( debug )
                    throw new Exception (messag);
                else
                    throw new NullReferenceException ( );
            }
        }

        public async Task Read( bool flag, bool debug, string queueListened){
            try{
                if ( inputMsg == null )
                    inputMsg = await PopOneMessageFromSQS (queueListened);
                await OpenExecuteAndSend (inputMsg, debug);
                //await TestOpenExecute();
                GC.Collect ( );
                GC.WaitForPendingFinalizers ( );
                var output = "      Result: Success.\t\t" + DateTime.Now.AddMilliseconds (-DateTime.Now.Millisecond).ToString ( ) + "\n";
                Console.WriteLine (output);
            }
            catch ( Exception e ){
                //Console.WriteLine(e);
                if ( e is NullReferenceException || e is ArgumentOutOfRangeException )
                    if ( flag ){ 
                        Console.Write ("\tEmpty, waiting...\r");
                    }
                    else
                        Console.Write ("\t                  \r");
                else{
                    Console.WriteLine ("\nERROR - STACKTRACE ----------------");
                    Console.WriteLine (e);
                    Console.WriteLine ("END OF STACKTRACE -----------------\n");
                }
                Thread.Sleep (500);
            }
            inputMsg = null;
            return;
        }
    }

    class Program{
        static async Task Main( string [] args ){
            bool debug = false;

            if ( args.Length > 0 ){
                if ( args [0] == "-d" || args [0] == "--debug" )
                    debug = true;
                else
                    Console.WriteLine ("Unknown argument. Only argument allowed: '-d' or '--debug' (enable debug mode).");
            }

            // Starting.
            Console.WriteLine ($"Starting service...");
            GoogleDocsReport gdreport = new GoogleDocsReport ( );

            // Selecting imput queues, both Azure and sqs.
            string queueListened = "geo-google-docs-dev";
            await gdreport.CreateIfNotExistAndUse (queueListened);

            // Preparing for test.
            // These instructions must be removed/commented in production.
            //await gdreport.CleanQueue();                                  //debug
            #if DEBUG 
                GoogleDocsReport.setTestMessage ( );                            //debug
            #endif
            
            Console.WriteLine ("");

            // Run forever and ever.
            bool flag = true;
            Run:
                await gdreport.Read (flag, debug, queueListened);
                flag = !flag;
            goto Run;
        }
    }
}
